/*!
@file		EigenUtil.cpp
@brief
@author		Yuji Oyamada
*/

#include <cassert>
#include <iostream>
#include <fstream>
#include <set>
#include <numeric>

#include <Eigen/Geometry>

#include "EigenUtil.hpp"

#ifdef __GNUC__
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wsign-conversion"
#endif

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, 1> SelectVector(
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& mat,
    const int index
)
{
    assert(
        mat.cols() > 0 &&
        index >= 0 &&
        index < mat.cols() &&
        "The specified index must be in range."
    );

    return mat.col(index);
}
// Explicit Instantiation for SelectColumn
template Eigen::Matrix<int, Eigen::Dynamic, 1> SelectVector<int>(
    const Eigen::MatrixXi&,
    const int
);
template Eigen::Matrix<float, Eigen::Dynamic, 1> SelectVector<float>(
    const Eigen::MatrixXf&,
    const int
);
template Eigen::Matrix<double, Eigen::Dynamic, 1> SelectVector<double>(
    const Eigen::MatrixXd&,
    const int
);

template <typename T, typename ContainerType>

Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> SelectVectors(
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& mat,
    const ContainerType& indices
)
{
    assert(typeid(*indices.begin()) == typeid(int&) &&
           "The indices must be specified by integer values");
    assert(mat.cols() > 0 &&
           indices.size() > 0 &&
           "The number of selected columns must be equal or smaller than the naumber of columns.");

    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> matSub(mat.rows(), indices.size());
    int n = 0;
    for(auto index: indices)
    {
        assert(
            0 <= index &&
            index < mat.cols() &&
            "The specified index must be in range."
        );
        matSub.col(n++) = mat.col(index);
    }

    return matSub;
}
// Explicit Instantiation for SelectColumn
template Eigen::MatrixXi SelectVectors<int, std::vector<int> >(const Eigen::MatrixXi&, const std::vector<int>&);
template Eigen::MatrixXi SelectVectors<int, std::set<int> >(const Eigen::MatrixXi&, const std::set<int>&);
template Eigen::MatrixXf SelectVectors<float, std::vector<int> >(const Eigen::MatrixXf&,     const std::vector<int>&);
template Eigen::MatrixXf SelectVectors<float, std::set<int> >(const Eigen::MatrixXf&, const std::set<int>&);
template Eigen::MatrixXd SelectVectors<double, std::vector<int> >(const Eigen::MatrixXd&,     const std::vector<int>&);
template Eigen::MatrixXd SelectVectors<double, std::set<int> >(const Eigen::MatrixXd&,     const std::set<int>&);

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> DeleteVectors(
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& mat,
    const int num_delete
)
{
    assert(
        mat.cols() > num_delete &&
        "Must keep at least 1 element");

    int size = int(mat.cols()) - num_delete;

    std::vector<int> indices(size);
    std::iota(indices.begin(), indices.end(), 0);

    return SelectVectors(mat, indices);
}

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> LoadPoints(
    const std::string& filename,
    const char delim
)
{
    std::vector< std::vector<T> > mat = LoadFile<T>(filename, delim);
    int cols = int(mat.size());
    int rows = int(mat[0].size());
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> points(rows, cols);
    for(int i = 0; i < cols; ++i)
    {
        assert(int(mat[i].size()) == rows &&
               "The points must be formated as matrix-like shape");
        for(int j = 0; j < rows; ++j)
        {
            points(j, i) = mat[i][j];
        }
    }

    return points;
}
template Eigen::MatrixXi LoadPoints<int>(const std::string&, const char);
template Eigen::MatrixXf LoadPoints<float>(const std::string&, const char);
template Eigen::MatrixXd LoadPoints<double>(const std::string&, const char);

template <typename T>
bool SavePoints(
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& mat,
    const std::string& filename,
    const char delim
)
{
    std::ofstream ofs(filename);
    for(int i = 0; i < mat.cols(); ++i)
    {
        for(int j = 0; j < mat.rows(); ++j)
        {
            ofs << mat(j, i);
            if((mat.rows()-j) != 1)
            {
                ofs << delim;
            }
            else
            {
                ofs << std::endl;
            }
        }
    }
    ofs.close();

    return true;
}
template bool SavePoints(const Eigen::MatrixXi&, const std::string&, const char);
template bool SavePoints(const Eigen::MatrixXf&, const std::string&, const char);
template bool SavePoints(const Eigen::MatrixXd&, const std::string&, const char);


template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> GenerateRandomPoints(
    const int dim,
    const int size,
    const T scale
)
{
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> points = (scale/2) * Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>::Random(dim, size);
    Eigen::Matrix<T, Eigen::Dynamic, 1> shift = (scale/2) * Eigen::Matrix<T, Eigen::Dynamic, 1>::Ones(dim, 1);
    points.colwise() += shift;
    return points;
}
template Eigen::MatrixXf GenerateRandomPoints(const int, const int, const float);
template Eigen::MatrixXd GenerateRandomPoints(const int, const int, const double);

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> ChangePoints(
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& _points,
    const int num_delete,
    const bool flagAffine,
    const T noise_scale,
    const bool flagPermutation
)
{
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> points = _points;

    if(num_delete != 0)
    {
        points = DeleteVectors(points, num_delete);
    }
    if(flagAffine)
    {
        points = Apply2dAffine(points);
    }
    if(noise_scale > T(0))
    {
        points = AddNoise(points, noise_scale);
    }
    if(flagPermutation)
    {
        points = PermutePoints(points);
    }
    return points;
}
template Eigen::MatrixXf ChangePoints(const Eigen::MatrixXf&, const int, const bool, const float, const bool);
template Eigen::MatrixXd ChangePoints(const Eigen::MatrixXd&, const int, const bool, const double, const bool);

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> Apply2dAffine(
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& _points
)
{
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> points = _points;
    T angle = static_cast<T>(rand()) / (static_cast<T>(M_PI));
    T scale = static_cast<T>(rand()) / (static_cast<T>(RAND_MAX/5.0));

    Eigen::Matrix<T, 2, 2> A = Eigen::Rotation2D<T>(angle) * Eigen::Scaling(scale);

    return A * _points;
}
template Eigen::MatrixXf Apply2dAffine(const Eigen::MatrixXf&);
template Eigen::MatrixXd Apply2dAffine(const Eigen::MatrixXd&);

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> AddNoise(
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& _points,
    const T noise_scale
)
{
    return _points + noise_scale * Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>::Random(_points.rows(), _points.cols());
}
template Eigen::MatrixXf AddNoise(const Eigen::MatrixXf&, const float);
template Eigen::MatrixXd AddNoise(const Eigen::MatrixXd&, const double);

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> PermutePoints(
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& _points
)
{
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> points = _points;

    int num_of_points = int(_points.cols());

    std::vector<int> indices(num_of_points);
    std::iota(indices.begin(), indices.end(), 0);
    std::random_shuffle(indices.begin(), indices.end());

    for(int i = 0; i < num_of_points; ++i)
    {
        points.col(indices[i]) = _points.col(i);
    }

    return points;
}
template Eigen::MatrixXf PermutePoints(const Eigen::MatrixXf&);
template Eigen::MatrixXd PermutePoints(const Eigen::MatrixXd&);

template <typename T>
std::vector<int> sort_indices(const std::vector<T>& v)
{
    std::vector<int> idx(v.size());
    std::iota(idx.begin(), idx.end(), 0);

    std::sort(idx.begin(),
              idx.end(),
              [&v](int i1, int i2) {return v[size_t(i1)] < v[size_t(i2)];});

    return idx;
}

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> SortPointsByAngle(
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& _points,
    const Eigen::Matrix<T, Eigen::Dynamic, 1>& base)
{
    long num_points = _points.cols();

    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> pointsRel = _points.colwise() - base;
    std::vector<T> angles(num_points);
    for(long i = 0; i < num_points; ++i)
    {
        T scale = pointsRel.array().abs().col(i).maxCoeff();
        T y = pointsRel(1, i) / scale;
        T x = pointsRel(0, i) / scale;
        angles[i] = T(180.0 * atan2(double(y), double(x)) / M_PI);
    }
    std::vector<int> indices = sort_indices(angles);

    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> points = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>(2, num_points);
    for(long i = 0; i < num_points; ++i)
    {
        points.col(i) = _points.col(indices[i]);
    }
    return points;
}
template Eigen::MatrixXf SortPointsByAngle(const Eigen::MatrixXf&, const Eigen::VectorXf&);
template Eigen::MatrixXd SortPointsByAngle(const Eigen::MatrixXd&, const Eigen::VectorXd&);

template <typename T>
T ComputeTriangleArea(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& points)
{
    assert(points.rows() == 2 &&
           points.cols() == 3 &&
           "The points must be 3 2D points.");

    int size = 3;
    std::vector<T> dist(size);
    T s = T(0.0);
    for(int i = 0; i < size; ++i)
    {
        dist[i] = (points.col(i) - points.col((i+1)%size)).norm();
        s += dist[i];
    }
    s *= T(0.5);

    return T(sqrt(double(s * (s-dist[0]) * (s-dist[1]) * (s-dist[2]))));
}
template float ComputeTriangleArea<float>(const Eigen::MatrixXf&);
template double ComputeTriangleArea<double>(const Eigen::MatrixXd&);

template <typename T>
T ComputeTriangleAreaRatio(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& points)
{
    assert(points.rows() == 2 &&
           points.cols() == 4 &&
           "The points must be 4 2D points.");

    T area012 = ComputeTriangleArea<T>(SelectVectors<T>(points, std::vector<int>({0, 1, 2})));
    T area123 = ComputeTriangleArea<T>(SelectVectors<T>(points, std::vector<int>({1, 2, 3})));
    return area012 / (area123 + std::numeric_limits<T>::epsilon());
}
template float ComputeTriangleAreaRatio(const Eigen::MatrixXf&);
template double ComputeTriangleAreaRatio(const Eigen::MatrixXd&);

#ifdef __GNUC__
    #pragma GCC diagnostic pop
#endif
