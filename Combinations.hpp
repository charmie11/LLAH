#ifndef COMBINATIONS_HPP
#define COMBINATIONS_HPP
/*!
 * \file Combinations.hpp
 *
 * \author Yuji Oyamada
 * \date 2018/08/24
 * \brief This file contains functions for combination/permutation.
 *
 */

#include <vector>

///
/// \brief nPk computes nPk.
/// \param _n The size of the set.
/// \param _k The size of the subset.
/// \return nPk.
///
int nPk(
    const int _n,
    const int _k
);

///
/// \brief nCk computes nCk.
/// \param _n The size of the set.
/// \param _k The size of the subset.
/// \return nCk.
///
int nCk(
    const int _n,
    const int _k
);

///
/// \brief BuildElements returns a std::vector<int> object that stores $n$ elements, whose i-th element's value is i.
///
/// In default, the first element is 0.
/// To start with arbitrary initial value, it's spicified by the 2nd argument valFirst.
/// \param numElement   The number of elements.
/// \return             The generated vector.
///
std::vector<int> BuildElements(
    const int numElement,
    const int valFirst = 0
);

///
/// \brief next_combination returns a next combination of nCk.
///
/// This function is copied from boost combination.
///
template <class BidirectionalIterator>
bool next_combination(
    BidirectionalIterator first1,
    BidirectionalIterator last1,
    BidirectionalIterator first2,
    BidirectionalIterator last2
);

///
/// \brief next_combination returns a next combination of nCk.
///
/// This function is copied from boost combination.
///
template <class BidirectionalIterator>
bool next_combination(
    BidirectionalIterator first,
    BidirectionalIterator middle,
    BidirectionalIterator last
);

///
/// \brief returns all combinations of nCr as a std::vector< std::vector<T> > object.
///
/// Given a set of elements \a elements[0], ..., \a elements[n-1],
/// the function returns their all nCk combinations \a all_combinations as std::vector< std::vector<T> > object as
/// \a all_combinations[0] = \a elements[0], ..., \a elements[k-1],
/// ...
/// \a all_combinations[nCk-1] = \a elements[n-k], ..., \a elements[n-1].
///
/// \param _elements The ground set containing all elements.
/// \param k The size of the subset.
/// \return all nCk combinations.
///
template <typename T>
std::vector< std::vector<T> > ComputeAllCombinations(
    const std::vector<T>& _elements,
    const int k
);

///
/// \brief returns all combinations of nCk as a std::vector< std::vector<T> > object.
///
/// Given \a n and \c k,
/// the function first build elements of integers \a elements = 0, ..., k-1
/// and then returns their all nCk combinations \a all_combinations as std::vector< std::vector<T> > object as
/// \a all_combinations[0] = \a elements[0], ..., \a elements[k-1],
/// ...
/// \a all_combinations[nCk-1] = \a elements[n-k], ..., \a elements[n-1].
///
/// \param n The size of the set.
/// \param k The size of the subset.
/// \return all nCk combinations.
///
std::vector< std::vector<int> > ComputeAllCombinations(
    const int n,
    const int k
);

///
/// \brief returns all permutations of nPk as a std::vector< std::vector<T> > object.
///
/// Given a set of elements \a elements[0], ..., \a elements[n-1],
/// the function returns their all nPk permutations \a all_permutations as std::vector< std::vector<T> > object as
/// \a all_permutations[0] = \a elements[0], ..., \a elements[k-1],
/// ...
/// \a all_permutations[nPk-1] = \a elements[n-1], ..., \a elements[n-k].
///
/// \param _elements The ground set containing all elements.
/// \param k The size of the subset.
/// \return all nPk permutations.
///
template <typename T>
std::vector< std::vector<T> > ComputeAllPermutationsSlow(
    const std::vector<T>& elements,
    const int k
);

///
/// \brief returns all permutations of nPk as a std::vector< std::vector<T> > object.
///
/// Given a set of elements \a elements[0], ..., \a elements[n-1],
/// the function returns their all nPk permutations \a all_permutations as std::vector< std::vector<T> > object as
/// \a all_permutations[0] = \a elements[0], ..., \a elements[k-1],
/// ...
/// \a all_permutations[nPk-1] = \a elements[n-1], ..., \a elements[n-k].
///
/// \param _elements The ground set containing all elements.
/// \param k The size of the subset.
/// \return all nPk permutations.
///
template <typename T>
std::vector< std::vector<T> > ComputeAllPermutations(
    const std::vector<T>& elements,
    const int k
);

///
/// \brief returns all permutations of nPk as a std::vector< std::vector<T> > object.
///
/// Given \a n and \a k,
/// the function first build elements of integers \a elements = 0, ..., k-1 and
/// returns their all nPk permutations \a all_permutations as std::vector< std::vector<T> > object as
/// \a all_permutations[0] = \a elements[0], ..., \a elements[k-1],
/// ...
/// \a all_permutations[nPk-1] = \a elements[n-1], ..., \a elements[n-k].
///
/// \param _elements The ground set containing all elements.
/// \param k The size of the subset.
/// \return all nPk permutations.
///
std::vector< std::vector<int> > ComputeAllPermutations(
    const int n,
    const int k
);

#endif
