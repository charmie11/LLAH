#include <cassert>
#include <iostream>
#include <fstream>

#include "EigenUtil.hpp"
#include "LLAH.hpp"

#ifdef __GNUC__
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wsign-conversion"
#endif

std::vector<std::string> LoadListFromFile(
    const std::string dirname,
    const std::string listname
)
{
    assert(IsExist(dirname+listname) &&
        "The specified file must exist"
    );
    std::ifstream ifs(dirname+listname);
    std::string line;

    std::vector<std::string> filenames;
    while(std::getline(ifs, line))
    {// for each line
        if(IsExist(dirname+line))
        {
            filenames.push_back(dirname+line);
        }
    }
    return filenames;
}

int main(int argc, char** argv)
{
    std::string dirname("../data/sample/");
    std::string filename_ref(dirname + "points_ref.points");
    std::vector<std::string> filenames_query = LoadListFromFile(dirname, "points_query_txt.list");
    if(argc > 1){   dirname = argv[1];}
    else if(argc > 2){  filename_ref= dirname + argv[2];}
    else if(argc > 3){  filenames_query = LoadListFromFile(dirname, argv[3]);}
    std::string filename;

    // setup a LLAH matcher
    LLAH::Database<float> db;

    // Register points
    Eigen::MatrixXf points = LoadPoints<float>(filename_ref);
    std::cout << "detect " << points.cols() << " points " << std::endl;
    if(db.RegisterPoints(points))
    {
        std::cout << "Registered " << points.cols() << " points" << std::endl;
    }
    else
    {
        std::cout << "The reference image must have at least " << db.params.n << " points" << std::endl;
        return -1;
    }

    // Match points
    for(std::string filename_query: filenames_query)
    {
        points = LoadPoints<float>(filename_query);
        std::cout << "Match " << points.cols() << " points..." << std::endl;
        std::vector<LLAH::Match> matches = db.MatchPoints(points);
        filename = ChangeFileExtension(filename_query, ".matches");
        LLAH::Save(matches, filename);
    }

    return 0;
}

#ifdef __GNUC__
    #pragma GCC diagnostic pop
#endif
