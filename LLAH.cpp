/*!
@file		LLAH.cpp
@brief
@author		Yuji Oyamada
*/

#include <stddef.h>
#include <cassert>
#include <iostream>
#include <fstream>
#include <limits>

#include "EigenUtil.hpp"
#include "LLAH.hpp"
#include "Combinations.hpp"

#ifdef __GNUC__
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wsign-conversion"
#endif


namespace LLAH
{

///////////////////////////////////////
// struct Match
///////////////////////////////////////
Match::Match(int _id_ref, int _id_query):
    id_ref(_id_ref),
    id_query(_id_query)
{
}

Match::Match(const Match& rhs):
    id_ref(rhs.id_ref),
    id_query(rhs.id_query)
{
}

Match::~Match()
{
}

Match& Match::operator=(const Match&rhs)
{
    if(this != &rhs)
    {
        id_ref = rhs.id_ref;
        id_query = rhs.id_query;
    }
    return *this;
}

bool Save(
    const Match& match,
    const std::string& filename,
    const char delim
)
{
    std::ofstream ofs(filename);
    ofs << match.id_ref << delim << match.id_query << std::endl;
    ofs.close();

    return true;
}

std::vector<int> GetIds(
    const std::vector<Match>& matches,
    const std::string mode
)
{
    std::vector<int> ids;
    for(Match match: matches)
    {
        if(mode.compare("query") == 0)
        {
            ids.push_back(match.id_query);
        }
        else if(mode.compare("ref") == 0)
        {
            ids.push_back(match.id_ref);
        }
    }
    return ids;
}

bool Save(
    const std::vector<Match>& matches,
    const std::string& filename,
    const char delim
)
{
    std::ofstream ofs(filename);
    for(Match match: matches)
    {
        ofs << match.id_ref << delim << match.id_query << std::endl;
    }
    ofs.close();

    return true;

}

///////////////////////////////////////
// struct Parameter
///////////////////////////////////////
Parameter::Parameter(const int _n, const int _m, const int _qbit)
{
    assert(_m >= 4);
    assert(_n >= _m);

    Set(_n, _m, _qbit);
}

Parameter::~Parameter(){
}

Parameter::Parameter(const Parameter& rhs)
{
    assert(rhs.m >= 4);
    assert(rhs.n >= rhs.m);

    Set(rhs.n, rhs.m, rhs.qbit);
}

Parameter& Parameter::operator=(const Parameter& rhs)
{
    if(this != &rhs)
    {
        Set(rhs.n, rhs.m, rhs.qbit);
    }
    return *this;
}

bool Parameter::Set(const int _n, const int _m, const int _qbit)
{
    n = _n;
    m = _m;
    k = 4;
    qbit = _qbit;
    int size_of_unsigned = sizeof(unsigned);
    hashsize = int(pow(2, 8 * size_of_unsigned - qbit)) - 1;

    nCm = ComputeAllCombinations(int(n), int(m));
    mCk = ComputeAllCombinations(int(m), int(k));
    mPerms = std::vector< std::vector<int> >(m, std::vector<int>(m));
    for(int i = 0; i < m; ++i)
    {
        for(int j = 0; j < m; ++j)
        {
            mPerms[i][j] = (i + j) % m;
        }
    }
}

///////////////////////////////////////
// class Database
///////////////////////////////////////
template class Database<float>;
template class Database<double>;

template <typename T>
Database<T>::Database(const Parameter& _params):
    params(_params),
    BitWidth(16),
    BitMask(0x0000ffff)
{
}

template <typename T>
Database<T>::Database(const int _n, const int _m, const int _qbit):
    params(Parameter(_n, _m, _qbit)),
    BitWidth(16),
    BitMask(0x0000ffff)
{
}

template <typename T>
Database<T>::~Database()
{
}

template <typename T>
Database<T>::Database(const Database<T>& rhs):
    params(Parameter(rhs.params.n, rhs.params.m, rhs.params.qbit)),
    BitWidth(rhs.BitWidth),
    BitMask(rhs.BitMask)
{
}

template <typename T>
Database<T>& Database<T>::operator=(const Database<T>& rhs)
{
    if(this != &rhs)
    {
        params = Parameter(rhs.params.n, rhs.params.m, rhs.params.qbit);
        BitWidth = rhs.BitWidth;
        BitMask = rhs.BitMask;
    }
    return *this;
}

template <typename T>
bool Database<T>::SetParameters(const int _n, const int _m, const int _qbit)
{
    params.Set(_n, _m, _qbit);
}

template <typename T>
bool Database<T>::RegisterPoints(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& points)
{
    if(points.cols() <= params.n)
    {
        return false;
    }

    points_ref = points;

    num_of_points_ref = int(points_ref.cols());

    // Step 1: compute raw features and build the hash function
    std::vector<T> features = ConstructHashFunction(points_ref);

    // Step 2: register each feature (update hash table)
    ConstructHashTable(features);

    // Step 3: prepare matching result
    matches = std::vector<Match>(num_of_points_ref);
    return true;
}
template bool Database<float>::RegisterPoints(const Eigen::MatrixXf&);
template bool Database<double>::RegisterPoints(const Eigen::MatrixXd&);

template <typename T>
bool Database<T>::RegisterPoints(const std::string& filename)
{
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> points = LoadPoints<T>(filename);
    return RegisterPoints(points);
}
template bool Database<float>::RegisterPoints(const std::string&);
template bool Database<double>::RegisterPoints(const std::string&);

template <typename T>
std::vector<T> Database<T>::ConstructHashFunction(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& points)
{
    long num_of_features = points.cols() * params.nCm.size() * params.mCk.size() * params.mPerms.size();

    ComputeKnn(points);
    // compute all raw features
    std::vector<T> features(num_of_features);
    int idFeature = 0;
    for(long idPoint = 0; idPoint < points.cols(); ++idPoint)
    {// for each point
        auto KnnIndex = std::vector<int>(KnnIndices[idPoint].begin(), KnnIndices[idPoint].begin()+params.n);
        auto Pn = SortPointsByAngle<T>(SelectVectors<T>(points, KnnIndex),
                                       points.col(int(idPoint)));
        for(auto tuple_m: params.nCm)
        {
            auto _Pm = SelectVectors<T>(Pn, tuple_m);
            for(auto mPerm: params.mPerms)
            {
                auto Pm = SelectVectors<T>(_Pm, mPerm);
                for(auto tuple_k: params.mCk)
                {
                    auto Pk = SelectVectors<T>(Pm, tuple_k);
                    features[idFeature++] = ComputeTriangleAreaRatio(Pk);
                }
            }
        }
    }

    // build hash functions
    Hfunc.clear();
    std::vector<T> featuresSorted(features);
    std::sort(featuresSorted.begin(), featuresSorted.end());

    int qsize = int(pow(2, params.qbit));
    T step = T(featuresSorted.size()) / T(qsize);

    for(int i = 0; i < qsize; ++i)
    {
        int pos = int(step*T(i));
        T val = featuresSorted[pos];
        Hfunc.push_back(val);
    }

    return features;

}
template std::vector<float> Database<float>::ConstructHashFunction(const Eigen::MatrixXf&);
template std::vector<double> Database<double>::ConstructHashFunction(const Eigen::MatrixXd&);

template <typename T>
bool Database<T>::ConstructHashTable(const std::vector<T>& features)
{
    int idFeature = 0;
    unsigned int id = 0;

    for(unsigned int idPoint = 0; idPoint < num_of_points_ref; ++idPoint)
    {// for each point
        id = idPoint;
        for(size_t i = 0; i < params.nCm.size(); ++i)
        {
            for(size_t j = 0; j < params.mPerms.size(); ++j)
            {
                long HashIndex = 0;
                for(std::vector<int> tuple_k: params.mCk)
                {
                    T feature = features[idFeature++];
                    HashIndex = HashIndex << params.qbit;
                    HashIndex += HashFunction(feature);
                    if(HashIndex > int(params.hashsize)){
                        HashIndex = HashIndex % int(params.hashsize);
                    }
                }

                // update Hash table
                auto itTable = HashTable.find(HashIndex);
                if(itTable == HashTable.end())
                {// this (marker, point) is not registered yet
                    std::vector<unsigned int> tmp;
                    tmp.push_back(id);
                    HashTable.insert(std::pair<long, std::vector<unsigned int>>(HashIndex, tmp));
                }
                else
                {// this (marker, point) is not registered yet
                    std::vector<unsigned int> &tmp = itTable->second;
                    auto it = std::find(tmp.begin(), tmp.end(), id);
                    if(it == tmp.end())
                    {// this id is not registered yet
                        tmp.push_back(id);
                    }
                }
            }
        }
    }
}
template bool Database<float>::ConstructHashTable(const std::vector<float>&);
template bool Database<double>::ConstructHashTable(const std::vector<double>&);

template <typename T>
int Database<T>::HashFunction(const T feature) const
{
    auto it = lower_bound(Hfunc.begin(), Hfunc.end(), feature);
    if(it != Hfunc.end())
    {
        return int(std::distance(Hfunc.begin(), it));
    }
    else
    {
        return int(Hfunc.size());
    }
}
template int Database<float>::HashFunction(const float feature) const;
template int Database<double>::HashFunction(const double feature) const;

template <typename T>
std::vector<Match> Database<T>::MatchPoints(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& points)
{
    matches.clear();
    if(points.cols() > params.n)
    {
        ComputeKnn(points);

        for(long i = 0; i < points.cols(); ++i)
        {// for each point
            std::vector<int> KnnIndex = std::vector<int>(KnnIndices[i].begin(), KnnIndices[i].begin()+int(params.n));
            auto Pn = SortPointsByAngle<T>(SelectVectors(points, KnnIndex),
                                           points.col(i));
            std::vector<unsigned int> ids;
            for(std::vector<int> tuple_m: params.nCm)
            {
                auto Pm = SelectVectors(Pn, tuple_m);
                long HashIndex = 0;
                for(std::vector<int> tuple_k: params.mCk)
                {
                    auto Pk = SelectVectors<T>(Pm, tuple_k);
                    T feature = ComputeTriangleAreaRatio(Pk);
                    HashIndex = HashIndex << params.qbit;
                    HashIndex += HashFunction(feature);
                    if(HashIndex > int(params.hashsize)){
                        HashIndex = HashIndex % int(params.hashsize);
                    }
                }
                auto itTable = HashTable.find(HashIndex);
                if(itTable != HashTable.end())
                {
                    std::vector<unsigned int> ids_each = itTable->second;
                    ids.insert(ids.end(), ids_each.begin(), ids_each.end());
                }
            }
            if(!ids.empty())
            {
                matches.push_back(Match(FindMostFrequent(ids), int(i)));
            }
            else
            {
                matches.push_back(Match(-1, int(i)));
            }
        }
    }

    return matches;
}
template std::vector<Match> Database<float>::MatchPoints(const Eigen::MatrixXf&);
template std::vector<Match> Database<double>::MatchPoints(const Eigen::MatrixXd&);

template <typename T>
std::vector<Match> Database<T>::MatchPoints(const std::string& filename)
{
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> points = LoadPoints<T>(filename);
    return MatchPoints(points);
}
template std::vector<Match> Database<float>::MatchPoints(const std::string&);
template std::vector<Match> Database<double>::MatchPoints(const std::string&);

template <typename T>
void Database<T>::ComputeKnn(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& points)
{
    int size = int(points.cols());
    KnnDistances = std::vector< std::vector<T> >(size, std::vector<T>(size));
    KnnIndices = std::vector< std::vector<int> >(size, std::vector<int>(size-1));

    using Pair = std::pair<float, int>;
    for(int i = 0; i < size; ++i)
    {
        // compute distance betweewn points[i] and other points
        KnnDistances[i][i] = std::numeric_limits<T>::infinity();
        for(int j = i+1; j < size; ++j)
        {
            T distance = (points.col(i) - points.col(j)).norm();
            KnnDistances[i][j] = distance;
            KnnDistances[j][i] = distance;
        }

        // set pair
        std::vector<Pair> data(size);
        for(int j = 0; j < size; ++j)
        {
            data[j] = Pair(KnnDistances[i][j], j);
        }
        std::sort(data.begin(), data.end(),
                  [](const Pair lhs, const Pair rhs) -> bool { return lhs.first < rhs.first;});

        // set index
        for(int j = 0; j < size-1; ++j)
        {
            KnnIndices[i][j] = data[j].second;
        }
    }
}

} // end of namespace LLAH

#ifdef __GNUC__
    #pragma GCC diagnostic pop
#endif
