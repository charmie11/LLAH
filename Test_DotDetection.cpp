#include <cassert>
#include <iostream>
#include <sstream>

#include "opencv2/opencv.hpp"

#include "DotDetection.hpp"

#ifdef __GNUC__
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wsign-conversion"
#endif

bool CaptureSequense(void)
{

    cv::VideoCapture cap(0);
    if(!cap.isOpened())
    {
        return -1;
    }

    cv::Mat img;
    cv::namedWindow("camera", 1);
    std::stringstream filename;
    int index = 0;
    bool flagCapture = true;
    while(flagCapture)
    {
        cap >> img;
        cv::imshow("camera", img);
        switch(cv::waitKey(1))
        {
        case 32: // Space
            std::cout << "Capture " << index << std::endl;
            filename.str("");
            filename.clear(std::stringstream::goodbit);
            filename << "../data/image" << (index++) << ".png";
            cv::imwrite(filename.str(), img);
            break;
        case 27: // ESC
            std::cout << "ESC" << std::endl;
            flagCapture = false;
            break;
        default:
            cv::waitKey(1);
        }
    }
    cv::destroyAllWindows();
}

int main()
{
    std::vector<std::string> filenames = {"../data/circle_grid.jpg",
                                          "../data/blob0.png",
                                          "../data/blob1.png",
                                          "../data/blob2.png",
                                          "../data/blob3.png"
                                         };
    std::string filename_txt;
    for(std::string filename_img: filenames)
    {
        filename_txt = filename_img;
        filename_txt.erase(filename_txt.end()-4, filename_txt.end());
        filename_txt = filename_txt + ".points";
        std::vector<Dot> dots = DetectDots(filename_img, filename_txt);
        for(Dot dot: dots)
        {
            std::cout << "(" << dot.x << "," << dot.y << ")" << std::endl;
        }
    }

    return 0;
}

#ifdef __GNUC__
    #pragma GCC diagnostic pop
#endif
