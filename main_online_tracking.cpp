#include <cassert>
#include <iostream>

#include "opencv2/opencv.hpp"

#include "PointsDetection.hpp"
#include "EigenUtil.hpp"
#include "LLAH.hpp"

#ifdef __GNUC__
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wsign-conversion"
#endif

int main(int argc, char** argv)
{
    std::string dirname("../data/");
    if(argc == 2)
    {
        dirname = argv[1];
    }
    std::string filename;

    // setup a LLAH matcher
    LLAH::Database<float> db;

    // setup a camera
    cv::VideoCapture cap(0);
    if(!cap.isOpened())
    {
        return -1;
    }
    std::string windowname = "camera";
    cv::namedWindow(windowname, 1);
    cv::Mat img, img_match;
    int index = 0;

    bool flag_capture = true;
    bool flag_db = false;
    const std::string message_db = "Press SPACE key to register a marker!";

    while(flag_capture)
    {
        cap >> img;
        cv::imshow(windowname, img);
        Eigen::MatrixXf points = DetectPoints<float>(img);
        std::cout << "detect " << points.cols() << " points " << std::endl;
        if(!flag_db)
        { // TODO: show the message on the image
            std::cout << message_db << std::endl;
            switch(cv::waitKey(1))
            {
            case 32: // Space
            // register points
                // register the detected points
                if(db.RegisterPoints(points))
                {
                    flag_db = true;
                    std::cout << "Registered " << points.cols() << " points" << std::endl;

                    // save the captured image
                    filename = dirname + "points_ref.png";
                    cv::imwrite(filename, img);

                    // save the detected points
                    filename = ChangeFileExtension(filename, ".points");
                    SavePoints<float>(points, filename);
                }
                break;
            case 27: // ESC
                std::cout << "ESC" << std::endl;
                flag_capture = false;
                break;
            default:
                cv::waitKey(1);
            }
        }
        else
        { // match the detected points
            std::cout << "Match " << points.cols() << " points..." << std::endl;
            std::vector<LLAH::Match> matches = db.MatchPoints(points);
            img_match = DrawMatchedPoints(img, points,
                                          LLAH::GetIds(matches, "ref"),
                                          LLAH::GetIds(matches, "query"));
            cv::imshow(windowname, img_match);

            switch(cv::waitKey(1))
            {
            case 32: // Space
                std::cout << "Run matching..." << std::endl;
                filename = dirname + "points_query" + std::to_string(index++) + ".png";
                // save the captured image
                cv::imwrite(filename, img);

                // save the captured points
                filename = ChangeFileExtension(filename, ".points");
                SavePoints<float>(points, filename);

                // save the matching indices
                filename = ChangeFileExtension(filename, ".matches");
                LLAH::Save(matches, filename);

                break;
            case 27: // ESC
                std::cout << "ESC" << std::endl;
                flag_capture = false;
                break;
            default:
                cv::waitKey(1);
            }
        }
    }
    cv::destroyAllWindows();

    return 0;
}

#ifdef __GNUC__
    #pragma GCC diagnostic pop
#endif
