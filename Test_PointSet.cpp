#include <cassert>
#include <iostream>
#include <fstream>
#include <limits>

#include "Util.hpp"
#include "Marker.hpp"

#ifdef __GNUC__
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wsign-conversion"
#endif

std::vector<int> GenerateIndices(std::mt19937 engine, const int size_all, const int size)
{
    std::vector<int> indices(size_all);
    std::iota(indices.begin(), indices.end(), 0);

    std::shuffle(indices.begin(), indices.end(), engine);

    return std::vector<int>(indices.begin(), indices.begin()+size);
}

template <typename T>
bool TestSelectColumn(std::mt19937 engine)
{
    int dim = 3;
    int size_all = 8;

    std::uniform_int_distribution<int> unidist(0, size_all-1);
    int index = unidist(engine);

    Eigen::Matrix <T, Eigen::Dynamic, Eigen::Dynamic> mat = Eigen::Matrix <T, Eigen::Dynamic, Eigen::Dynamic>::Random(dim, size_all);
    auto vec = SelectColumn<T>(mat, index);
    assert(vec == mat.col(index) &&
           "The selected column is wrongly selected.");

    return true;
}

template <typename T>
bool TestSelectColumns(std::mt19937 engine)
{
    int dim = 3;
    int size_all = 8;
    int size = 2;

    std::vector<int> indices = GenerateIndices(engine, size_all, size);

    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> mat = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>::Random(dim, size_all);
    auto submat = SelectColumns<T>(mat, indices);
    for(size_t i = 0; i < indices.size(); ++i)
    {
        assert(submat.col(i) == mat.col(indices[i]) &&
               "The selected column is wrongly selected.");
    }

    return true;
}

template <typename T>
bool CompareTwoPointSets(const PointSet<T>& P0,
                         const PointSet<T>& P1)
{
    // check matrices
    assert(P0.Points().rows() == P1.Points().rows() &&
           P0.Points().cols() == P1.Points().cols() &&
           "Two PointSet must be save size");
    assert((P0.Points() - P1.Points()).norm() <= std::numeric_limits<T>::epsilon() &&
           "Two PointSet must be same");

    int num_of_points = P0.NumOfPoints();
    // check Knn indices
    assert(P0.Knn().size() == P1.Knn().size() &&
           "Two PointSet must have same KNN indices information");
    for(int i = 0; i < num_of_points; ++i)
    {
        assert(P0.Knn(i).size() == P1.Knn(i).size() &&
               "Two PointSet must have same KNN indices information");
        for(int j = 0; j < int(P0.Knn(i).size()); ++j)
        {
            assert(P0.Knn(i)[j] == P1.Knn(i)[j] &&
                   "Two PointSet must have same KNN indices information");
        }
    }

    // check Knn distance
    assert(P0.Distance().size() == P1.Distance().size() &&
           "Two PointSet must have same KNN distance information");
    for(int i = 0; i < num_of_points; ++i)
    {
        assert(P0.Distance(i).size() == P1.Distance(i).size() &&
               "Two PointSet must have same KNN distance information");
        for(int j = 0; j < int(P0.Distance(i).size()); ++j)
        {
            if(i != j)
            {
                assert(fabs(double(P0.Distance(i)[j] - P1.Distance(i)[j])) <= std::numeric_limits<double>::epsilon() &&
                       "Two PointSet must have same KNN distance information");
            }
            else
            {
                assert(std::isinf(P0.Distance(i)[j]) &&
                       "Two PointSet must have same KNN distance information");

            }
        }
    }
}

template <typename T>
bool TestPointSet(void)
{
    int dim = 2;
    int size = 8;
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> points = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>::Random(dim, size);

    PointSet<T> P0;
    P0.Set(points);
    PointSet<T> P1(points);
    PointSet<T> P2(P1);

    CompareTwoPointSets(P0, P1);
    CompareTwoPointSets(P1, P2);
    CompareTwoPointSets(P2, P0);

    return true;
}

template <typename T>
bool TestPointSetConcat(void)
{
    int dim = 2;
    int size = 8;
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> points = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>::Random(dim, size);
    PointSet<T> P0(SelectColumns<T>(points, std::vector<int>({0, 1, 2})));
    PointSet<T> P1(SelectColumns<T>(points, std::vector<int>({3, 4, 5, 6, 7})));
    PointSet<T> P;

    // test operator+
    P = P0 + P1;
    assert(P.Points().rows() == points.rows() &&
           P.Points().cols() == points.cols() &&
           "Two PointSet must be save size");
    assert((P.Points() - points).norm() <= std::numeric_limits<T>::epsilon() &&
           "Two PointSet must be same");

    // test operator+=
    P0 += P1;
    assert(P0.Points().rows() == points.rows() &&
           P0.Points().cols() == points.cols() &&
           "Two PointSet must be save size");
    assert((P0.Points() - points).norm() <= std::numeric_limits<T>::epsilon() &&
           "Two PointSet must be same");

    return true;
}

template <typename T>
bool TestPointSetIO(void)
{
    PointSet<T> P0("../data/test.points");

    PointSet<T> P1(P0.Points());
    P1.Save("../data/copy.points");

    CompareTwoPointSets(P0, P1);

    return true;
}

int main()
{
    std::srand(static_cast<unsigned int>(time(nullptr)));

    std::random_device seed_gen;
    std::mt19937 engine(seed_gen());

    // test for PointSet IO
    {
        TestPointSetIO<float>();
        std::cout << "TestPointSetIO<int>: passed" << std::endl;
        TestPointSetIO<double>();
        std::cout << "TestPointSetIO<double>: passed" << std::endl;
    }

    // test for SelectColumn(s)
    {
        TestSelectColumn<int>(engine);
        std::cout << "TestSelectColumn<int>: passed" << std::endl;
        TestSelectColumn<float>(engine);
        std::cout << "TestSelectColumn<float>: passed" << std::endl;
        TestSelectColumn<double>(engine);
        std::cout << "TestSelectColumn<double>: passed" << std::endl;

        TestSelectColumns<int>(engine);
        std::cout << "TestSelectColumns<int>: passed" << std::endl;
        TestSelectColumns<float>(engine);
        std::cout << "TestSelectColumns<float>: passed" << std::endl;
        TestSelectColumns<double>(engine);
        std::cout << "TestSelectColumns<double>: passed" << std::endl;
    }

    // test for PointSet struct
    {
        TestPointSet<float>();
        std::cout << "TestPointSet<float>: passed" << std::endl;
        TestPointSet<double>();
        std::cout << "TestPointSet<double>: passed" << std::endl;
        TestPointSetConcat<float>();
        std::cout << "TestPointSetConcat<float>: passed" << std::endl;
        TestPointSetConcat<double>();
        std::cout << "TestPointSetConcat<double>: passed" << std::endl;
    }

    // test for PointSet loader
    {
    }
    return 0;
}

#ifdef __GNUC__
    #pragma GCC diagnostic pop
#endif
