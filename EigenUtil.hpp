#ifndef EIGENUTIL_HPP
#define EIGENUTIL_HPP
/*!
 * \file EigenUtil.hpp
 *
 * \author Yuji Oyamada
 * \date 2018/08/30
 * \brief This file contains functions for Eigen utility functions.
 *
 */

// headers for declaration

#include <Eigen/Core>

#include "Util.hpp"

///
/// \brief selectColumn selects a column vector given a matrix.
/// \param mat      The original matrix.
/// \param index    The index of the selected column.
/// \return         The \c index-th column of \c mat.
///
template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, 1> SelectVector(
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& mat,
    const int index
);

///
/// \brief SelectColumns selects a set of column vector given a matrix as another matrix.
/// \param mat      The original matrix.
/// \param indices  The indices of the selected column given as an STL container.
/// \return         A set of the \c index-th element of \c mat.
///
template <typename T, typename ContainerType>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> SelectVectors(
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& mat,
    const ContainerType& indices
);

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> DeleteVectors(
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& mat,
    const int num_delete=0
);

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> LoadPoints(
    const std::string& filename,
    const char delim=','
);

template <typename T>
bool SavePoints(
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& mat,
    const std::string& filename,
    const char delim=','
);

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> GenerateRandomPoints(
    const int dim,
    const int size,
    const T scale
);

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> ChangePoints(
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& _points,
    const int num_delete=0,
    const bool flagAffine=false,
    const T noise_scale=T(0),
    const bool flagPermutation=false
);

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> Apply2dAffine(
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& _points
);

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> AddNoise(
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& _points,
    const T noise_scale
);

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> PermutePoints(
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& _points
);

///////////////////////////////////////
// functions for feature description
///////////////////////////////////////
template <typename T>
std::vector<int> sort_indices(const std::vector<T>& v);

///
/// \brief sorts a set of points based on their relative angles against the base point.
/// \param _points The all points.
/// \param base The base point.
/// \return The sorted points.
///
template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> SortPointsByAngle(
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& _points,
    const Eigen::Matrix<T, Eigen::Dynamic, 1>& base
);

///
/// \brief computes the area of a triangle.
/// \param points The 3 points of a triangle.
/// \return The area of the triangle.
///
/// d01 = |p0-p1|
/// d12 = |p1-p2|
/// d20 = |p2-p0|
/// s = 0.5*(d01 + d12 + d20)
/// area = sqrt(s*(s-d01)*(s-d12)*(s-d20))
template <typename T>
T ComputeTriangleArea(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& points);

///
/// \brief computes the area ratio of two triangles.
/// \param points The 4 points of a triangle.
/// \return area(p0, p1, p2) / area(p1, p2, p3)
///
template <typename T>
T ComputeTriangleAreaRatio(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& points);

#endif
