#include <cassert>
#include <iostream>
#include <sstream>
#include <fstream>
#include <map>
#include <algorithm>
#include "Util.hpp"

template <typename T>
//! prints a std::vector object.
void Print(const std::vector<T>& vec)
{
    for(T x: vec)
    {
        std::cout << x << ", ";
    }
}
template void Print(const std::vector<int>&);
template void Print(const std::vector<float>&);
template void Print(const std::vector<double>&);

template <typename T>
//! prints a std::vector<std::vector> object.
void Print(const std::vector< std::vector<T> >& mat)
{
    for(std::vector<T> vec: mat)
    {
        for(T x: vec)
        {
            std::cout << x << ", ";
        }
        std::cout << std::endl;
    }
}
template void Print(const std::vector< std::vector<int> >&);
template void Print(const std::vector< std::vector<float> >&);
template void Print(const std::vector< std::vector<double> >&);

//! checks whether \c filename exists.
bool IsExist(
    const std::string& filename
)
{
    std::ifstream ifs(filename.c_str() );
    if(ifs.fail())
    {
        return false;
    }
    ifs.close();

    return true;
}

template <typename T>
std::vector< std::vector<T> > LoadFile(
    const std::string& filename,
    const char delim
)
{
    assert(IsExist(filename) &&
        "The specified file must exist"
    );
    std::ifstream ifs(filename);
    std::string line, word;
    std::vector< std::vector<T> > points;

    std::vector<T> vec;
    while(std::getline(ifs, line))
    {// for each line
        vec.clear();
        std::istringstream iss(line);
        while(std::getline(iss, word, delim))
        {
            vec.push_back(T(std::stod(word)));
        }
        points.push_back(vec);
    }
    return points;
}
// Explicit Instantiation for LoadFile
template std::vector< std::vector<int> > LoadFile(const std::string&, const char);
template std::vector< std::vector<float> > LoadFile(const std::string&, const char);
template std::vector< std::vector<double> > LoadFile(const std::string&, const char);

template <typename T>
bool SaveFile(const std::vector< std::vector<T> >& mat,
     const std::string& filename,
     const char delim
)
{
    std::ofstream ofs(filename);
    for(std::vector<T> vec: mat)
    {
        for(T& x: vec)
        {
            ofs << x;
            if(&x != &vec.back())
            {
                ofs << delim;
            }
            else
            {
                ofs << std::endl;
            }
        }
    }
    ofs.close();

    return true;
}
// Explicit Instantiation for SaveFile
template bool SaveFile(const std::vector< std::vector<int> >&, const std::string&, const char);
template bool SaveFile(const std::vector< std::vector<float> >&, const std::string&, const char);
template bool SaveFile(const std::vector< std::vector<double> >&, const std::string&, const char);

template <typename T>
bool SaveFile(const std::vector<T>& vec,
     const std::string& filename
)
{
    std::ofstream ofs(filename);
    for(T x: vec)
    {
        ofs << x << std::endl;
    }
    ofs.close();

    return true;
}
// Explicit Instantiation for SaveFile
template bool SaveFile(const std::vector<int>&, const std::string&);
template bool SaveFile(const std::vector<float>&, const std::string&);
template bool SaveFile(const std::vector<double>&, const std::string&);

std::string ChangeFileExtension(
    const std::string str,
    const std::string ext
)
{
    return str.substr(0, str.find_last_of('.')) + ext;
}

template<typename T>
T FindMostFrequent(std::vector<T>& elements)
{
    std::map<T, int> histogram;
    for(T element: elements)
    {
        if(histogram.find(element) != histogram.end())
        {
            ++histogram[element];
        }
        else
        {
            histogram[element] = 1;
        }
    }
    return std::max_element(histogram.begin(), histogram.end(),
            [] (const std::pair<T, int>& pair1, const std::pair<T, int>& pair2) {
            return pair1.second < pair2.second;})->first;
}
template int FindMostFrequent(std::vector<int>&);
template unsigned int FindMostFrequent(std::vector<unsigned int>&);
