#ifndef POINTSDETECTION_HPP
#define POINTSDETECTION_HPP
/*!
 * \file PointsDetection.hpp
 *
 * \author Yuji Oyamada
 * \date 2018/09/10
 * \brief This file contains functions for dot detection from image using OpenCV
 *
 */

// headers for declaration
#include <stddef.h>
#include <vector>
#include <string>

#include <Eigen/Core>
#include <opencv2/core.hpp>

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> DetectPoints(const cv::Mat& img);

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> DetectPoints(const std::string& filename);

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> DetectPoints(
    const cv::Mat& img,
    const std::string& filename_txt,
    const char delim=','
);

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> DetectPoints(
    const std::string& filename_img,
    const std::string& filename_txt,
    const char delim=','
);

template <typename T>
cv::Mat DrawMatchedPoints(
    const cv::Mat& _img,
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& points,
    const std::vector<int>& ids_ref,
    const std::vector<int>& ids_query
);

#endif
