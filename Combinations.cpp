/*!
 * \file Combinations.cpp
 *
 * \author Yuji Oyamada
 * \date 2015/01/21
 * \brief This file contains functions for combination/permutation.
 *
 */

#include "Combinations.hpp"

// headers for definition
#include <cmath>
#include <cassert>
#include <algorithm>
#include <numeric>
#include <vector>
#ifdef _OPENMP
    #include <omp.h>
#endif

#ifdef __GNUC__
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wsign-conversion"
#endif

int nPk(
    const int _n,
    const int _k
)
{
    assert(_n >= _k &&
           "n >= k to compute nPk.");

    int val = 1;
    int n = _n;
    int k = _k;

    while(k)
    {
        val *= n;
        --n;
        --k;
    }

    return val;
}

int nCk(
    const int _n,
    const int _k
)
{
    assert(_n >= _k &&
           "n >= k to compute nCk.");

    int n = _n;
    int k = std::min(_k, _n-_k);

    return nPk(n, k) / nPk(k, k);
}

std::vector<int> BuildElements(
    const int numElement,
    const int valFirst
)
{
    assert(numElement > 0 &&
           "The number of elements must be positive integer.");

    std::vector<int> elements(numElement);
    std::iota(elements.begin(), elements.end(), valFirst);

    return elements;
}

template <class BidirectionalIterator>
bool next_combination(
    BidirectionalIterator first1,
    BidirectionalIterator last1,
    BidirectionalIterator first2,
    BidirectionalIterator last2
)
{
    if( ( first1 == last1 ) || ( first2 == last2 ) )
    {
        return false;
    }
    BidirectionalIterator m1 = last1;
    BidirectionalIterator m2 = last2; --m2;
    while(--m1 != first1 && !(*m1 < *m2 ))
    {
    }
    bool result = (m1 == first1 ) && !(* first1 < *m2 );
    if(!result)
    {
        while(first2 != m2 && !(*m1 < *first2))
        {
            ++first2;
        }
        first1 = m1;
        std::iter_swap (first1 , first2 );
        ++first1;
        ++first2;
    }
    if( (first1 != last1) && (first2 != last2))
    {
        m1 = last1; m2 = first2;
        while( (m1 != first1) && (m2 != last2))
        {
            std::iter_swap(--m1, m2);
            ++m2;
        }
        std::reverse(first1, m1);
        std::reverse(first1, last1);
        std::reverse(m2, last2);
        std::reverse(first2, last2);
    }
    return !result;
}

template <class BidirectionalIterator>
bool next_combination(
    BidirectionalIterator first,
    BidirectionalIterator middle,
    BidirectionalIterator last
)
{
    return next_combination(first, middle, middle, last);
}

template <typename T>
std::vector< std::vector<T> > ComputeAllCombinations(
    const std::vector<T>& _elements,
    const int k
)
{
    int n = int(_elements.size());
    assert(n >= k &&
           "n >= k to compute nPk.");

    std::vector<T> combination(k);
    std::vector< std::vector<T> > all_combinations(nCk(n, k), combination);
    std::vector<T> elements(_elements);
    std::sort(elements.begin(), elements.end());

    int id = 0;
    do
    {
        for(int i = 0; i < k; ++i)
        {
            combination[i] = elements[i];
        }
        std::sort(combination.begin(), combination.end());
        all_combinations[id] = combination;
        ++id;
    }
    while(next_combination(elements.begin(), elements.begin()+k, elements.end()));

    return all_combinations;
}
// Explicit Instantiation for ComputeAllCombinations
template std::vector< std::vector<int> > ComputeAllCombinations(
    const std::vector<int>&,
    const int k
);
template std::vector< std::vector<float> > ComputeAllCombinations(
    const std::vector<float>&,
    const int k
);
template std::vector< std::vector<double> > ComputeAllCombinations(
    const std::vector<double>&,
    const int k
);

std::vector< std::vector<int> > ComputeAllCombinations(
    const int n,
    const int k
)
{
    return ComputeAllCombinations(BuildElements(n), k);
}

template <typename T>
std::vector< std::vector<T> > ComputeAllPermutationsSlow(
    const std::vector<T>& _elements,
    const int k
)
{
    int n = int(_elements.size());
    assert(n >= k &&
           "n >= k to compute nPk.");
    std::vector<T> permutation;
    std::vector< std::vector<T> > all_permutations;
    std::vector<T> elements(n);
    // elem must be sorted before computing all permutations
    for(int i = 0; i < n; ++i)
    {
        elements[i] = _elements[i];
    }
    std::sort(elements.begin(), elements.end());
    do
    {
        // check whether the indices already registered or not
        permutation.clear();
        for(int i = 0; i < k; ++i)
        {
            permutation.push_back(elements[i]);
        }
        all_permutations.push_back(permutation);
        //
    } while(std::next_permutation(elements.begin(), elements.end()));

    return all_permutations;
}
// Explicit Instantiation for ComputeAllPermutationsSlow
template std::vector< std::vector<int> > ComputeAllPermutationsSlow(
    const std::vector<int>&,
    const int k
);
template std::vector< std::vector<float> > ComputeAllPermutationsSlow(
    const std::vector<float>&,
    const int k
);
template std::vector< std::vector<double> > ComputeAllPermutationsSlow(
    const std::vector<double>&,
    const int k
);

template <typename T>
std::vector< std::vector<T> > ComputeAllPermutations(
    const std::vector<T>& _elements,
    const int k
)
{
    int n = int(_elements.size());
    assert(n >= k &&
           "n >= k to compute nPk.");
    std::vector< std::vector<T> > all_combinations = ComputeAllCombinations(_elements, k);
    std::vector<int> elements_perm = BuildElements(k);
    std::vector< std::vector<int> > all_permutations_kPk = ComputeAllPermutationsSlow(elements_perm, k);
    int num_of_nCk = nCk(n, k);
    int num_of_nPk = nPk(n, k);
    int num_of_kPk = nPk(k, k);
    std::vector< std::vector<T> > all_permutations(num_of_nPk, std::vector<T>(k));
    std::vector<T> permutation(k);
    #ifdef _OPENMP
    #pragma omp parallel for private(permutation)
    #endif
    for(int ic = 0; ic < num_of_nCk; ++ic)
    {
        for(int ii = 0; ii < num_of_kPk; ++ii)
        {
            permutation = std::vector<T>(k);
            for(int n = 0; n < k; ++n)
            {
                permutation[n] = all_combinations[ic][all_permutations_kPk[ii][n]];
            }
            all_permutations[ic*num_of_kPk+ii] = permutation;
        }
    }
    return all_permutations;
}
// Explicit Instantiation for ComputeAllPermutations
template std::vector< std::vector<int> > ComputeAllPermutations(
    const std::vector<int>&,
    const int
);
template std::vector< std::vector<float> > ComputeAllPermutations(
    const std::vector<float>&,
    const int
);
template std::vector< std::vector<double> > ComputeAllPermutations(
    const std::vector<double>&,
    const int
);

std::vector< std::vector<int> > ComputeAllPermutations(
    const int n,
    const int k
)
{
    return ComputeAllPermutations(BuildElements(n), k);
}

#ifdef __GNUC__
    #pragma GCC diagnostic pop
#endif
