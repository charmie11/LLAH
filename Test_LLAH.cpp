#include <iostream>
#include <ctime>
#include <random>

#include "EigenUtil.hpp"
#include "LLAH.hpp"

#ifdef __GNUC__
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wsign-conversion"
#endif

void TestRetrieval(const int mode)
{
    switch(mode)
    {
    case 0:
        std::cout << "  Affine transform" << std::endl;
        break;
    case 1:
        std::cout << "  Noisy" << std::endl;
        break;
    case 2:
        std::cout << "  Permutation" << std::endl;
        break;
    case 3:
        std::cout << "  Reduction" << std::endl;
        break;
    default:
        std::cout << "  Default" << std::endl;
    }

    int num_of_points = 30;

    // synthesize a set of random markers
    Eigen::MatrixXf P = GenerateRandomPoints<float>(2, num_of_points, 256.0f);

    // build a database
    LLAH::Database<float> db;
    db.RegisterPoints(P);

    // run retrieval
    Eigen::MatrixXf Q;
    switch(mode)
    {
    case 0: // affine transform
        Q = Apply2dAffine(P);
        break;
    case 1: // add noise
        Q = AddNoise(P, 2.0f);
        break;
    case 2: // permute points
        Q = PermutePoints(P);
        break;
    case 3: // delete elements
        Q = DeleteVectors(P, 5);
        break;
    default:
        Q = P;
    }
    std::vector<LLAH::Match> matches = db.MatchPoints(Q);
    for(int j = 0; j < Q.cols(); ++j)
    {
        std::cout << "Q[" << j << "] <-> ";
        if(matches[j].id_ref != -1)
        {
            switch(mode)
            {
                break;
            case 2:
                std::cout << "diff = " << (Q.col(j) - P.col(matches[j].id_ref)).norm() << std::endl;
                break;
            default:
                std::cout << "P[" << matches[j].id_ref << "]" << std::endl;
            }
        }
        else
        {
            std::cout << "Not match" << std::endl;
        }
    }
}

int main()
{
    std::srand(static_cast<unsigned int>(time(nullptr)));

    int num_modes = 4;
    for(int i = 0; i < num_modes; ++i)
    {
        TestRetrieval(i);
    }

    return 0;
}

#ifdef __GNUC__
    #pragma GCC diagnostic pop
#endif
