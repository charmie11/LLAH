#ifndef UTIL_HPP
#define UTIL_HPP
/*!
 * \file Util.hpp
 *
 * \author Yuji Oyamada
 * \date 2018/09/05
 * \brief This file contains functions for some utilities.
 *
 */

// headers for declaration
#include <stddef.h>
#include <vector>
#include <string>

template <typename T>
//! prints a std::vector object.
void Print(const std::vector<T>& vec);

template <typename T>
//! prints a std::vector<std::vector> object.
void Print(const std::vector< std::vector<T> >& mat);

//! checks whether \c filename exists.
bool IsExist(const std::string& filename);

template <typename T>
std::vector< std::vector<T> > LoadFile(
    const std::string& filename,
    const char delim=','
);

template <typename T>
bool SaveFile(
    const std::vector< std::vector<T> >& mat,
    const std::string& filename,
    const char delim=','
);

template <typename T>
bool SaveFile(
    const std::vector<T>& vec,
    const std::string& filename
);

std::string ChangeFileExtension(
    const std::string str,
    const std::string ext
);

template<typename T>
T FindMostFrequent(std::vector<T>& elements);

#endif
