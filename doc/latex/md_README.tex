This is a simplified C++ implementation of Locally Likely Arrangement Hashing (\hyperlink{namespace_l_l_a_h}{L\+L\+AH}) that is used for image retrieval and object tracking. Contrast to typical image features such as S\+I\+FT and O\+RB, \hyperlink{namespace_l_l_a_h}{L\+L\+AH} computes feature vectors from keypoints distribution not from image texture although keypoints are detected from texture. So, \hyperlink{namespace_l_l_a_h}{L\+L\+AH} should technically work with arbitrary appropriate keypoints detector.

The original \hyperlink{namespace_l_l_a_h}{L\+L\+AH} was proposed by \href{http://imlab.jp/LLAH/}{\tt Nakai et al.} My implementation is based on Uchiyama\textquotesingle{}s \href{http://limu.ait.kyushu-u.ac.jp/~uchiyama/me/code/UCHIYAMARKERS/index.html}{\tt code}. In the code, \hyperlink{namespace_l_l_a_h}{L\+L\+AH} uses an affine invariant, the ratio of two triangles\textquotesingle{} area, as its base geometric feature. This means that
\begin{DoxyItemize}
\item \hyperlink{namespace_l_l_a_h}{L\+L\+AH} (the code) cannot work with severe perspective transformation.
\item \hyperlink{namespace_l_l_a_h}{L\+L\+AH} solves points\textquotesingle{} permutation problem in the algorithm.
\end{DoxyItemize}

\subsection*{Dependency}

\subsubsection*{External dependency}

This implementation heavily depends on \href{http://eigen.tuxfamily.org/index.php?title=Main_Page}{\tt Eigen} library.
\begin{DoxyItemize}
\item Required\+: Eigen (3.\+2.\+92)
\item Optional\+: Open\+CV (3.\+4.\+2)
\end{DoxyItemize}

\subsubsection*{Tested environment}


\begin{DoxyItemize}
\item Eigen (3.\+2.\+92), Open\+CV (3.\+4.\+2) on Ubuntu 16.\+04 64bit
\end{DoxyItemize}

\subsection*{How to use}

\subsubsection*{How to in C++ code}

{\itshape \hyperlink{class_l_l_a_h_1_1_database}{L\+L\+A\+H\+::\+Database}} class offers \hyperlink{namespace_l_l_a_h}{L\+L\+AH} functionalities that register reference points and match query points to the registered reference. The following code shows how to register reference and match query. The matching result is returned as a {\itshape std\+::vector$<$\hyperlink{struct_l_l_a_h_1_1_match}{L\+L\+A\+H\+::\+Match}$>$} object. 
\begin{DoxyCode}
1 LLAH::Database<float> db;
2 
3 // register reference points
4 Eigen::MatrixXf points\_ref = LoadPoints<float>(filename\_ref);
5 db.RegisterPoints(points\_ref);
6 
7 // match query points to the reference
8 Eigen::MatrixXf points\_query = LoadPoints<float>(filename\_query);
9 std::vector<LLAH::Match> matches = db.MatchPoints(points);
10 
11 // save and show the results
12 LLAH::Save(matches, filename\_matches);
13 for(auto match: matches)
14 \{
15     std::cout << "reference[" << match.id\_ref << "] <-> query[" << match.id\_query << "]" << std::endl;
16 \}
\end{DoxyCode}


Executing the above code, you will see the following message on the terminal. As you may notice, some reference points are assigned to several query points. 
\begin{DoxyCode}
1 reference[7] <-> query[0]
2 reference[3] <-> query[1]
3 reference[0] <-> query[2]
4 reference[10] <-> query[3]
5 reference[7] <-> query[4]
6 reference[8] <-> query[5]
7 reference[1] <-> query[6]
8 reference[8] <-> query[7]
9 reference[1] <-> query[8]
10 reference[12] <-> query[9]
11 reference[15] <-> query[10]
\end{DoxyCode}


\subsubsection*{Offline tracking with text formated points}

Run \hyperlink{main__offline__tracking__from__files_8cpp}{main\+\_\+offline\+\_\+tracking\+\_\+from\+\_\+files.\+cpp}. 
\begin{DoxyCode}
1 ./build/LLAH\_Offline\_from\_file DIRNAME REFNAME LISTNAME
\end{DoxyCode}
 {\itshape D\+I\+R\+N\+A\+ME} is a directory where all files are stored. {\itshape D\+I\+R\+N\+A\+ME}+\+\_\+\+R\+E\+F\+N\+A\+M\+E\+\_\+ contains a pointset such that each line contains a 2D point information with a delimiter (default is \textquotesingle{},\textquotesingle{}). {\itshape D\+I\+R\+N\+A\+ME}+\+\_\+\+L\+I\+S\+T\+N\+A\+M\+E\+\_\+ contains a set of filenames, each of which contains a query pointset.

\subsubsection*{Offline tracking with image formated points (Requires Open\+CV)}

Run \hyperlink{main__offline__tracking__from__images_8cpp}{main\+\_\+offline\+\_\+tracking\+\_\+from\+\_\+images.\+cpp}. 
\begin{DoxyCode}
1 ./build/LLAH\_Offline\_from\_image DIRNAME REFNAME LISTNAME
\end{DoxyCode}
 {\itshape D\+I\+R\+N\+A\+ME} is a directory where all files are stored. {\itshape D\+I\+R\+N\+A\+ME}+\+\_\+\+R\+E\+F\+N\+A\+M\+E\+\_\+ contains an image of reference pointset. {\itshape D\+I\+R\+N\+A\+ME}+\+\_\+\+L\+I\+S\+T\+N\+A\+M\+E\+\_\+ contains a set of filenames, each of which is an image of query pointset.

\subsubsection*{Online tracking (Requires Open\+CV)}

Connect a camera to your PC and then run \hyperlink{main__online__tracking_8cpp}{main\+\_\+online\+\_\+tracking.\+cpp}. 
\begin{DoxyCode}
1 ./build/LLAH\_Online DIRNAME
\end{DoxyCode}
 {\itshape D\+I\+R\+N\+A\+ME} is a directory where all files are stored.

\subsection*{How to improve the tracking performance}

\hyperlink{namespace_l_l_a_h}{L\+L\+AH} uses the distribution of neighboring points as mentioned above. Most important parameters of \hyperlink{namespace_l_l_a_h}{L\+L\+AH} is {\itshape n} and {\itshape m} (see Sec. 2.\+4 of Nakai2006 for detail). Simply speaking, larger {\itshape n($>$m)} and smaller {\itshape m($>$4)} improves the tracking performance against perspective transformation. Note that larger {\itshape n} increases computation time exponentially. Empirically, their default values are set as {\itshape n = 8, m = 5}. You can set these parameters via constructor as 
\begin{DoxyCode}
1 int n = 9;
2 int m = 5;
3 LLAH::Database<float> db(n, m);
\end{DoxyCode}


\subsection*{References}

If you use the code or part of the code in your work, please cite the following papers. 
\begin{DoxyCode}
1 @inproceedings\{Nakai2006,
2   title=\{Use of affine invariants in locally likely arrangement hashing for camera-based document image
       retrieval\},
3   author=\{T. Nakai and K. Kise and M. Iwamura\},
4   booktitle=\{International Workshop DAS\}
5   pages=\{541--552\},
6   year=\{2006\}
7 \}
\end{DoxyCode}



\begin{DoxyCode}
1 @inproceedings\{Uchiyama2011,
2   title=\{Random dot markers\},
3   author=\{Hideaki Uchiyama and Hideo Saito\},
4   booktitle=\{IEEE Virtual Reality (VR)\},
5   pages=\{35--38\},
6   year=\{2011\}
7 \}
\end{DoxyCode}
 