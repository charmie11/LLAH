var searchData=
[
  ['m',['m',['../struct_l_l_a_h_1_1_parameter.html#ad9d803242ce3b030a20607f9477656bf',1,'LLAH::Parameter']]],
  ['main',['main',['../main_8cpp.html#ae66f6b31b5ad750f1fe042a706a4e3d4',1,'main():&#160;main.cpp'],['../main__offline__tracking__from__files_8cpp.html#a3c04138a5bfe5d72780bb7e82a18e627',1,'main(int argc, char **argv):&#160;main_offline_tracking_from_files.cpp'],['../main__offline__tracking__from__images_8cpp.html#a3c04138a5bfe5d72780bb7e82a18e627',1,'main(int argc, char **argv):&#160;main_offline_tracking_from_images.cpp'],['../main__online__tracking_8cpp.html#a3c04138a5bfe5d72780bb7e82a18e627',1,'main(int argc, char **argv):&#160;main_online_tracking.cpp'],['../_test___combinations_8cpp.html#ae66f6b31b5ad750f1fe042a706a4e3d4',1,'main():&#160;Test_Combinations.cpp'],['../_test___dot_detection_8cpp.html#ae66f6b31b5ad750f1fe042a706a4e3d4',1,'main():&#160;Test_DotDetection.cpp'],['../_test___l_l_a_h_8cpp.html#ae66f6b31b5ad750f1fe042a706a4e3d4',1,'main():&#160;Test_LLAH.cpp'],['../_test___point_set_8cpp.html#ae66f6b31b5ad750f1fe042a706a4e3d4',1,'main():&#160;Test_PointSet.cpp']]],
  ['main_2ecpp',['main.cpp',['../main_8cpp.html',1,'']]],
  ['main_5foffline_5ftracking_5ffrom_5ffiles_2ecpp',['main_offline_tracking_from_files.cpp',['../main__offline__tracking__from__files_8cpp.html',1,'']]],
  ['main_5foffline_5ftracking_5ffrom_5fimages_2ecpp',['main_offline_tracking_from_images.cpp',['../main__offline__tracking__from__images_8cpp.html',1,'']]],
  ['main_5fonline_5ftracking_2ecpp',['main_online_tracking.cpp',['../main__online__tracking_8cpp.html',1,'']]],
  ['match',['Match',['../struct_l_l_a_h_1_1_match.html',1,'LLAH']]],
  ['match',['Match',['../struct_l_l_a_h_1_1_match.html#a9f020f47885387a8f9e3f13bf84145d5',1,'LLAH::Match::Match(int _id_ref=-1, int _id_query=-1)'],['../struct_l_l_a_h_1_1_match.html#a6e0f91c795721dbb8ca8de2d16a257ec',1,'LLAH::Match::Match(const Match &amp;rhs)']]],
  ['matches',['matches',['../class_l_l_a_h_1_1_database.html#a6e21f6dde6c268580bdc08ab93e045f6',1,'LLAH::Database']]],
  ['matchpoints',['MatchPoints',['../class_l_l_a_h_1_1_database.html#a7621a7638c49626a2deef78dcb34cf09',1,'LLAH::Database::MatchPoints(const Eigen::Matrix&lt; T, Eigen::Dynamic, Eigen::Dynamic &gt; &amp;points)'],['../class_l_l_a_h_1_1_database.html#ab5cc34d4dcbb0f10692b2855b0514707',1,'LLAH::Database::MatchPoints(const std::string &amp;filename)']]],
  ['mck',['mCk',['../struct_l_l_a_h_1_1_parameter.html#af6f9c2906018995e2fa19d835e55bf38',1,'LLAH::Parameter']]],
  ['mean',['mean',['../struct_statistics.html#a08cc0c0c573f2a44673177c8ad0e1962',1,'Statistics']]],
  ['mperms',['mPerms',['../struct_l_l_a_h_1_1_parameter.html#aab7fc328d66fe47752d0c89181d685f0',1,'LLAH::Parameter']]]
];
