var files =
[
    [ "Combinations.cpp", "_combinations_8cpp.html", "_combinations_8cpp" ],
    [ "Combinations.hpp", "_combinations_8hpp.html", "_combinations_8hpp" ],
    [ "EigenUtil.cpp", "_eigen_util_8cpp.html", "_eigen_util_8cpp" ],
    [ "EigenUtil.hpp", "_eigen_util_8hpp.html", "_eigen_util_8hpp" ],
    [ "LLAH.cpp", "_l_l_a_h_8cpp.html", "_l_l_a_h_8cpp" ],
    [ "LLAH.hpp", "_l_l_a_h_8hpp.html", "_l_l_a_h_8hpp" ],
    [ "main.cpp", "main_8cpp.html", "main_8cpp" ],
    [ "main_offline_tracking_from_files.cpp", "main__offline__tracking__from__files_8cpp.html", "main__offline__tracking__from__files_8cpp" ],
    [ "main_offline_tracking_from_images.cpp", "main__offline__tracking__from__images_8cpp.html", "main__offline__tracking__from__images_8cpp" ],
    [ "main_online_tracking.cpp", "main__online__tracking_8cpp.html", "main__online__tracking_8cpp" ],
    [ "PointsDetection.cpp", "_points_detection_8cpp.html", "_points_detection_8cpp" ],
    [ "PointsDetection.hpp", "_points_detection_8hpp.html", "_points_detection_8hpp" ],
    [ "Test_Combinations.cpp", "_test___combinations_8cpp.html", "_test___combinations_8cpp" ],
    [ "Test_DotDetection.cpp", "_test___dot_detection_8cpp.html", "_test___dot_detection_8cpp" ],
    [ "Test_LLAH.cpp", "_test___l_l_a_h_8cpp.html", "_test___l_l_a_h_8cpp" ],
    [ "Test_PointSet.cpp", "_test___point_set_8cpp.html", "_test___point_set_8cpp" ],
    [ "Util.cpp", "_util_8cpp.html", "_util_8cpp" ],
    [ "Util.hpp", "_util_8hpp.html", "_util_8hpp" ]
];