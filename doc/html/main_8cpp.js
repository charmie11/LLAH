var main_8cpp =
[
    [ "Statistics", "struct_statistics.html", "struct_statistics" ],
    [ "ElapsedTime", "struct_elapsed_time.html", "struct_elapsed_time" ],
    [ "TestResult", "main_8cpp.html#a5a827c144574ce31cf6e2a131f666601", null ],
    [ "AppendResult", "main_8cpp.html#a37aad3b0110267a03871f84201c9a807", null ],
    [ "Compare", "main_8cpp.html#af5c1da075b98d18f6af9d76854ba37ed", null ],
    [ "main", "main_8cpp.html#ae66f6b31b5ad750f1fe042a706a4e3d4", null ],
    [ "PrintMatchings", "main_8cpp.html#a795e7b207c79b83c9209600f0f9ba533", null ],
    [ "SaveResults", "main_8cpp.html#a5f872bfc0823aeb849701dc908af4d84", null ]
];