var class_l_l_a_h_1_1_database =
[
    [ "Database", "class_l_l_a_h_1_1_database.html#ab6c806d92a21b88ace3b96c83b1f7951", null ],
    [ "Database", "class_l_l_a_h_1_1_database.html#ab37aa69dc4e5200ba078033520131e81", null ],
    [ "~Database", "class_l_l_a_h_1_1_database.html#ab3b7a0537b9fcb9d74198bf8083bfe3e", null ],
    [ "Database", "class_l_l_a_h_1_1_database.html#a6b5b68401251c1000350f52a5eb47a35", null ],
    [ "MatchPoints", "class_l_l_a_h_1_1_database.html#a7621a7638c49626a2deef78dcb34cf09", null ],
    [ "MatchPoints", "class_l_l_a_h_1_1_database.html#ab5cc34d4dcbb0f10692b2855b0514707", null ],
    [ "operator=", "class_l_l_a_h_1_1_database.html#ac7911add7cd5996b5f57277b1182e4b5", null ],
    [ "RegisterPoints", "class_l_l_a_h_1_1_database.html#a0259264c27b7327976c2ea736b6a531c", null ],
    [ "RegisterPoints", "class_l_l_a_h_1_1_database.html#a41b57dfa177f2119a0eb18fc592b722e", null ],
    [ "SetParameters", "class_l_l_a_h_1_1_database.html#ad0b09739a6b5278c6dc6dbd386bd940f", null ],
    [ "matches", "class_l_l_a_h_1_1_database.html#a6e21f6dde6c268580bdc08ab93e045f6", null ],
    [ "num_of_points_ref", "class_l_l_a_h_1_1_database.html#a547caf87392ee276aa9e9ba3f6a4773c", null ],
    [ "params", "class_l_l_a_h_1_1_database.html#a6bfc3151924b9024f8e54b99f99bc1e5", null ],
    [ "points_ref", "class_l_l_a_h_1_1_database.html#a4cb6e364f992af28e7f9e4c7ffdcf2ba", null ]
];