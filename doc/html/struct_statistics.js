var struct_statistics =
[
    [ "Statistics", "struct_statistics.html#a60ddd90a571ed4c3ce8c0f6317a36d63", null ],
    [ "Statistics", "struct_statistics.html#a01eab06c86950cd1569fde887bbe98a2", null ],
    [ "~Statistics", "struct_statistics.html#ab68ede75479e44d5c35b78ec1284065b", null ],
    [ "Compute", "struct_statistics.html#abdd9b8a3792cdf1527304f1b0bf0239e", null ],
    [ "Print", "struct_statistics.html#a3883eb2ab90cd84845675d6c115c3df1", null ],
    [ "mean", "struct_statistics.html#a08cc0c0c573f2a44673177c8ad0e1962", null ],
    [ "stdev", "struct_statistics.html#a76cb8b566751289e973446a8d4e17f2c", null ],
    [ "valmax", "struct_statistics.html#ac1b9f591683728efb93e5e11c65a06a7", null ],
    [ "valmin", "struct_statistics.html#ad3b47a0660f33384d2f1ebc945073ad1", null ]
];