#ifndef LLAH_HPP
#define LLAH_HPP
/*!
 * \file LLAH.hpp
 *
 * \author Yuji Oyamada
 * \date 2018/08/24
 * \brief This file contains functions for LLAH.
 *
 */

// headers for declaration
#include <stddef.h>
#include <vector>
#include <map>

#include <Eigen/Core>

namespace LLAH
{

struct Match{
    Match(int _id_ref=-1, int _id_query=-1);
    Match(const Match& rhs);
    ~Match();
    Match& operator=(const Match& rhs);

    int id_ref;      //!< The id of reference
    int id_query;    //!< The id of query
};

std::vector<int> GetIds(
    const std::vector<Match>& matches,
    const std::string mode
);

bool Save(
    const Match& match,
    const std::string& filename,
    const char delim=','
);

bool Save(
    const std::vector<Match>& matches,
    const std::string& filename,
    const char delim=','
);

struct Parameter{
    Parameter(const int _n=8, const int _m=5, const int _qbit=4);
    ~Parameter();
    Parameter(const Parameter& rhs);
    Parameter& operator=(const Parameter& rhs);

    bool Set(const int _n, const int _m, const int _qbit);

    std::vector< std::vector<int> > nCm;
    std::vector< std::vector<int> > mCk;
    std::vector< std::vector<int> > mPerms;
    int n;		//!< n of nCm
    int m;		//!< m of nCm
    int k;		//!< k is 4 (for ratio of two triangles)
    int qbit;	//!< quantization level ( represented by number of bit)
    int hashsize;		//!< size of hash table (computed from qbit)

};

// class for Database
template <typename T>
class Database{
public:
    ///
    /// \brief The default constructor.
    /// \param _params LLAH parametes.
    ///
    Database(const Parameter& _params);

    ///
    /// \brief The default constructor.
    /// \param _n n of nCm
    /// \param _m m of nCm and mCk
    /// \param _qbit The number of bit for feature quantization.
    ///
    Database(const int _n=8, const int _m=5, const int _qbit=4);

    ///
    /// \brief Destructor
    ~Database();

    ///
    /// \brief Copy constructor
    /// \param rhs The original object to be copied.
    ///
    Database(const Database<T>& rhs);

    /// \brief operator=
    Database<T>&operator=(const Database<T>& rhs);

    ///
    /// \brief resets the LLAH parameters.
    /// \param _n n of nCm
    /// \param _m m of nCm and mCk
    /// \param _qbit The number of bit for feature quantization.
    bool SetParameters(const int _n, const int _m, const int _qbit);

    ///
    /// \brief register a reference Eigen::Matrix to LLAH.
    /// \param mat The reference Eigen::Matrix.
    bool RegisterPoints(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& mat);
    bool RegisterPoints(const std::string& filename);

    ///
    /// \brief finds a set of corresponding points given a pointset.
    /// \param points The query pointset.
    /// \return A set of (marker, point) IDs for all query points.
    std::vector<Match> MatchPoints(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& points);

    std::vector<Match> MatchPoints(const std::string& filename);

    unsigned int num_of_points_ref;
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> points_ref;
    Parameter params;
    std::vector<Match> matches;
private:

    ///
    /// \brief constructs a hash function given a set of reference markers.
    /// \param markers The set of reference markers.
    /// \return A set of features obtained from all points in the reference markers.
    std::vector<T> ConstructHashFunction(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& points);

    ///
    /// \brief constructs a hash table given a set of reference  markers and obtained its features.
    /// \param markers The set of reference markers.
    /// \param features The set of features obtained from \c markers.
    bool ConstructHashTable(const std::vector<T>& features);

    /// \brief computes a hash value given a feature value.
    /// \param feature The raw feature value.
    /// \return The hash value.
    int HashFunction(const T feature) const ;

    void ComputeKnn(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& points);

    std::vector<T> Hfunc;
    std::map<long, std::vector<unsigned int> > HashTable;
    int BitWidth;
    unsigned int BitMask;
    std::vector< std::vector<T> > KnnDistances; //!< The distance of all point pairs.
    std::vector< std::vector<int> > KnnIndices; //!< The indices of all kNN points.
};

} // end of namespace LLAH

#endif
