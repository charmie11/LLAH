#include <iostream>
#include <fstream>
#include <sstream>
#include <ctime>
#include <chrono>
#include <random>
#include <numeric>

#include "EigenUtil.hpp"
#include "LLAH.hpp"

#ifdef __GNUC__
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wsign-conversion"
#endif

struct Statistics
{
    Statistics(){}
    Statistics(const Statistics& rhs):
        mean(rhs.mean),
        stdev(rhs.stdev),
        valmin(rhs.valmin),
        valmax(rhs.valmax){}
    ~Statistics(){}

    template <typename T>
    void Compute(const std::vector<T>& vec)
    {
        double size = vec.size();
        mean = std::accumulate(vec.begin(), vec.end(), 0) / size;
        std::vector<double> diff(vec.size());
        std::transform(vec.begin(), vec.end(), diff.begin(), [this](double x) { return x - this->mean; });
        double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
        stdev = std::sqrt(sq_sum / size);
        valmin =* std::min_element(vec.begin(), vec.end());
        valmax =* std::max_element(vec.begin(), vec.end());
    }

    void Print(const std::string prefix)
    {
        std::cout << prefix << "Mean: " << mean << std::endl;
        std::cout << prefix << "Stdev: " << stdev << std::endl;
        std::cout << prefix << "Min: " << valmin << std::endl;
        std::cout << prefix << "Max: " << valmax << std::endl;
    }

    double mean;
    double stdev;
    double valmin;
    double valmax;
};

struct ElapsedTime
{
    ElapsedTime(const double _db=0.0, const double _find=0.0):
        db(_db),
        find(_find){}
    ElapsedTime(const ElapsedTime& rhs):
        db(rhs.db),
        find(rhs.find){}
    ~ElapsedTime(){}

    double db;
    double find;
};

using TestResult = std::tuple<LLAH::Parameter, float, Statistics, ElapsedTime>;

void PrintMatchings(const std::vector<int>& matchings)
{
    for(size_t i = 0; i < matchings.size(); ++i)
    {
        std::cout << "  query[" << i << "] <-> ";
        if(matchings[i] >= 0)
        {
            std::cout << "reference[" << matchings[i] << "]" << std::endl;
        }
        else
        {
            std::cout << "Not matched" << std::endl;
        }
    }
}

void SaveResults(
    const std::vector<TestResult>& results,
    const std::string& filename
)
{
    std::ofstream ofs(filename);
    ofs << "n,m,noise,,mean,stdev,min,max,epalsed time for database construction [ms], elapsed time for retrieval [ms]" << std::endl;
    for(TestResult result: results)
    {
        ofs << std::get<0>(result).n << ","; // n of LLAH parameter
        ofs << std::get<0>(result).m << ","; // m of LLAH parameter
        ofs << std::get<1>(result) << ",,"; // noise scale
        ofs << std::get<2>(result).mean << ",";   // mean of multiple tests
        ofs << std::get<2>(result).stdev << ",";  // standard deviation of multiple tests
        ofs << std::get<2>(result).valmin << ","; // min value of multiple tests
        ofs << std::get<2>(result).valmax << ","; // max value of multiple tests
        ofs << std::get<3>(result).db << ",";        // elapsed time for database construction
        ofs << std::get<3>(result).find << std::endl; // elapsed time for retrieval
    }
    ofs.close();
}

void AppendResult(
    const TestResult& result,
    const std::string& filename
)
{
    bool flagNew = !IsExist(filename);
    std::ofstream ofs(filename, std::ofstream::app);
    if(flagNew)
    {
        ofs << "n,m,noise,,mean,stdev,min,max,epalsed time for database construction [ms], elapsed time for retrieval [ms]" << std::endl;
    }
    ofs << std::get<0>(result).n << ","; // n of LLAH parameter
    ofs << std::get<0>(result).m << ","; // m of LLAH parameter
    ofs << std::get<1>(result) << ",,"; // noise scale
    ofs << std::get<2>(result).mean << ",";   // mean of multiple tests
    ofs << std::get<2>(result).stdev << ",";  // standard deviation of multiple tests
    ofs << std::get<2>(result).valmin << ","; // min value of multiple tests
    ofs << std::get<2>(result).valmax << ","; // max value of multiple tests
    ofs << std::get<3>(result).db << ",";        // elapsed time for database construction
    ofs << std::get<3>(result).find << std::endl; // elapsed time for retrieval
    ofs.close();
}

int Compare(
    const std::vector<int>& v0,
    const std::vector<int>& v1
)
{
    assert(v0.size() == v1.size());
    int count = 0;
    for(int i = 0; i < int(v0.size()); ++i)
    {
        if(v0[i] == v1[i])
        {
            ++count;
        }
    }
    return count;
}

int main()
{
    std::srand(static_cast<unsigned int>(time(nullptr)));


    // build LLAH database
    std::cout << "building LLAH database..." << std::endl;
//    std::vector<LLAH::Parameter> params = {LLAH::Parameter(7, 5),
//                                           LLAH::Parameter(7, 6),
//                                           LLAH::Parameter(8, 5),
//                                           LLAH::Parameter(8, 6),
//                                           LLAH::Parameter(8, 7),
//                                           LLAH::Parameter(9, 5),
//                                           LLAH::Parameter(9, 6),
//                                           LLAH::Parameter(9, 7),
//                                           LLAH::Parameter(9, 8)};
    std::vector<LLAH::Parameter> params = {LLAH::Parameter(7, 5),
                                           LLAH::Parameter(7, 6),
                                           LLAH::Parameter(8, 5)};

    int num_of_trial = 10;
    int num_of_points = 50;
    std::vector<int> matchings_GT(num_of_points);
    std::iota(matchings_GT.begin(), matchings_GT.end(), 0);
    std::cout << "Start " << num_of_trial << " test..." << std::endl;

    std::vector<TestResult> results;
    std::chrono::system_clock::time_point time_start, time_end;
    ElapsedTime et;
    for(LLAH::Parameter param: params)
    { // for each LLAH parameter
        std::cout << "LLAH(" << param.n << ", " << param.m << ")" << std::endl << std::endl;
        Eigen::MatrixXf points_ref = GenerateRandomPoints(2, num_of_points, 256.0f);

        time_start = std::chrono::system_clock::now();
        LLAH::Database<float> db(param);
        db.RegisterMarkers(points_ref);
        time_end = std::chrono::system_clock::now();
        et.db = std::chrono::duration_cast<std::chrono::milliseconds>(time_end-time_start).count();

        for(float noise_scale = 0.0f; noise_scale < 3.0f; noise_scale += 0.5f)
        { // for each noise scale

            time_start = std::chrono::system_clock::now();
            // run test several times
            std::vector<int> num_of_matches;
            for(int t = 0; t < num_of_trial; ++t)
            {
                std::cout << "\e[A\r\e[0K" << "  Noise " << noise_scale << "  " << t << "/" << num_of_trial << std::endl;
                auto points_query = ChangePoints<float>(points_ref, 0, true, noise_scale);
                std::vector<int> matchings = db.Find(points_query);
                num_of_matches.push_back(Compare(matchings_GT, matchings));
            }
            time_end = std::chrono::system_clock::now();
            et.find = std::chrono::duration_cast<std::chrono::milliseconds>(time_end-time_start).count() / double(num_of_trial);

            // compute statistics
            Statistics stat;
            stat.Compute(num_of_matches);
            stat.Print("    ");
            std::cout << std::endl;

            TestResult result = std::make_tuple(param, noise_scale, stat, et);
            results.push_back(result);
            AppendResult(result, "../data/results_random_test.log");
        }
    }
    SaveResults(results, "../data/results_random_test_small.log");

    return 0;
}

#ifdef __GNUC__
    #pragma GCC diagnostic pop
#endif
