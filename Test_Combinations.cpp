#include <cassert>
#include <iostream>
#include <algorithm>

#include "Util.hpp"
#include "Combinations.hpp"

#ifdef __GNUC__
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wsign-conversion"
#endif

template <typename T>
std::vector< std::vector<T> > GroundTruth5C3(void)
{
    return std::vector< std::vector<T> >({{0, 1, 2}, {0, 1, 3}, {0, 1, 4}, {0, 2, 3}, {0, 2, 4}, {0, 3, 4},
                                          {1, 2, 3}, {1, 2, 4}, {1, 3, 4},
                                          {2, 3, 4}});
}

bool TestComputeAllCombinations(void)
{
    // test 1: 5C3 for integer
    auto GT_i = GroundTruth5C3<int>();
    assert(ComputeAllCombinations(5, 3) == GT_i);
    assert(ComputeAllCombinations(std::vector<int>({0, 1, 2, 3, 4}), 3) == GT_i);

    // test 2: 5C3 for float
    auto GT_f = GroundTruth5C3<float>();
    assert(ComputeAllCombinations(std::vector<float>({0, 1, 2, 3, 4}), 3) == GT_f);

    // test 3: 5C3 for double
    auto GT_d = GroundTruth5C3<double>();
    assert(ComputeAllCombinations(std::vector<double>({0, 1, 2, 3, 4}), 3) == GT_d);

    return true;
}


template <typename T>
std::vector< std::vector<T> > GroundTruth5P3(void)
{
    return std::vector< std::vector<T> >({{0, 1, 2}, {0, 2, 1}, {1, 0, 2}, {1, 2, 0}, {2, 0, 1}, {2, 1, 0},
                                          {0, 1, 3}, {0, 3, 1}, {1, 0, 3}, {1, 3, 0}, {3, 0, 1}, {3, 1, 0},
                                          {0, 1, 4}, {0, 4, 1}, {1, 0, 4}, {1, 4, 0}, {4, 0, 1}, {4, 1, 0},
                                          {0, 2, 3}, {0, 3, 2}, {2, 0, 3}, {2, 3, 0}, {3, 0, 2}, {3, 2, 0},
                                          {0, 2, 4}, {0, 4, 2}, {2, 0, 4}, {2, 4, 0}, {4, 0, 2}, {4, 2, 0},
                                          {0, 3, 4}, {0, 4, 3}, {3, 0, 4}, {3, 4, 0}, {4, 0, 3}, {4, 3, 0},
                                          {1, 2, 3}, {1, 3, 2}, {2, 1, 3}, {2, 3, 1}, {3, 1, 2}, {3, 2, 1},
                                          {1, 2, 4}, {1, 4, 2}, {2, 1, 4}, {2, 4, 1}, {4, 1, 2}, {4, 2, 1},
                                          {1, 3, 4}, {1, 4, 3}, {3, 1, 4}, {3, 4, 1}, {4, 1, 3}, {4, 3, 1},
                                          {2, 3, 4}, {2, 4, 3}, {3, 2, 4}, {3, 4, 2}, {4, 2, 3}, {4, 3, 2}});
}

bool TestComputeAllPermutations(void)
{
    // test 1: 5P3 for integer
    auto GT_i = GroundTruth5P3<int>();
    Print(GT_i);
    assert(ComputeAllPermutations(5, 3) == GT_i);
    assert(ComputeAllPermutations(std::vector<int>({0, 1, 2, 3, 4}), 3) == GT_i);

    // test 2: 5P3 for float
    auto GT_f = GroundTruth5P3<float>();
    assert(ComputeAllPermutations(std::vector<float>({0, 1, 2, 3, 4}), 3) == GT_f);

    // test 3: 5P3 for double
    auto GT_d = GroundTruth5P3<double>();
    assert(ComputeAllPermutations(std::vector<double>({0, 1, 2, 3, 4}), 3) == GT_d);

    return true;
}

int main()
{
    TestComputeAllCombinations();
    std::cout << "TestComputeAllCombinations: passed" << std::endl;

    TestComputeAllPermutations();
    std::cout << "TestComputeAllPermutations: passed" << std::endl;

    return 0;
}

#ifdef __GNUC__
    #pragma GCC diagnostic pop
#endif
