#include <cassert>
#include <iostream>
#include <sstream>
#include <fstream>

#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>

#include "PointsDetection.hpp"
#include "EigenUtil.hpp"

#ifdef __GNUC__
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wsign-conversion"
#endif

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> DetectPoints(const cv::Mat& _img)
{
    cv::Mat img;

    if(_img.channels() == 1)
    { // _img is gray
        img = _img.clone();
    }
    else
    { // _img is color
        cv::cvtColor(_img, img, cv::COLOR_BGR2GRAY);
    }

    // prepare blob detector
    cv::SimpleBlobDetector::Params param;
    param.filterByCircularity = true;
    param.minCircularity = 0.85f;
    param.maxCircularity = 1.0f;
    param.filterByArea = true;
    param.minArea = float(pow(10.0, 2.0));
    param.maxArea = float(pow(100.0, 2.0));
    param.filterByInertia = true;
    param.minInertiaRatio = 0.5f;
    param.maxInertiaRatio = 1.0f;

    // detect blobs
    auto detector = cv::SimpleBlobDetector::create(param);
    std::vector<cv::KeyPoint> keypoints;
    detector->detect(img, keypoints);

    // convert keypoints to 2d std::vector<T>
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> points = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>::Zero(2, keypoints.size());
    for(int i = 0; i < keypoints.size(); ++i)
    {
        points(0, i) = T(keypoints[i].pt.x);
        points(1, i) = T(keypoints[i].pt.y);
    }

    return points;
}
template Eigen::MatrixXf DetectPoints(const cv::Mat&);
template Eigen::MatrixXd DetectPoints(const cv::Mat&);

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> DetectPoints(const std::string& filename)
{
    assert(IsExist(filename) &&
           "The specified image must exist");

    cv::Mat img = cv::imread(filename, cv::IMREAD_GRAYSCALE);

    return DetectPoints<T>(img);
}
template Eigen::MatrixXf DetectPoints(const std::string&);
template Eigen::MatrixXd DetectPoints(const std::string&);

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> DetectPoints(
    const cv::Mat& img,
    const std::string& filename_txt,
    const char delim)
{
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> points = DetectPoints<T>(img);
    SavePoints<T>(points, filename_txt, delim);

    return points;
}
template Eigen::MatrixXf DetectPoints(const cv::Mat&, const std::string&, const char);
template Eigen::MatrixXd DetectPoints(const cv::Mat&, const std::string&, const char);

template <typename T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> DetectPoints(
    const std::string& filename_img,
    const std::string& filename_txt,
    const char delim)
{
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> points = DetectPoints<T>(filename_img);
    SavePoints<T>(points, filename_txt, delim);

    return points;
}
template Eigen::MatrixXf DetectPoints(const std::string&, const std::string&, const char);
template Eigen::MatrixXd DetectPoints(const std::string&, const std::string&, const char);

template <typename T>
cv::Mat DrawMatchedPoints(
    const cv::Mat& _img,
    const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& points,
    const std::vector<int>& ids_ref,
    const std::vector<int>& ids_query
)
{
    assert(ids_ref.size() == ids_query.size());

    cv::Mat img;

    if(_img.channels() == 1)
    { // _img is gray
        cv::cvtColor(_img, img, cv::COLOR_GRAY2BGR);
    }
    else
    { // _img is color
        img = _img.clone();
    }

    for(int i = 0; i < ids_query.size(); ++i)
    {
        cv::Point point(int(points(0, i)), int(points(1, i)));
        if(ids_ref[i] >= 0)
        {
            cv::putText(img, std::to_string(ids_ref[i]), point, cv::FONT_HERSHEY_SIMPLEX, 1.2, cv::Scalar(0, 0, 255), 2, CV_AA);
        }
    }

    return img;
}
template cv::Mat DrawMatchedPoints(const cv::Mat&, const Eigen::MatrixXf&, const std::vector<int>&, const std::vector<int>&);
template cv::Mat DrawMatchedPoints(const cv::Mat&, const Eigen::MatrixXd&, const std::vector<int>&, const std::vector<int>&);

#ifdef __GNUC__
    #pragma GCC diagnostic pop
#endif
