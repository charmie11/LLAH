#include <cassert>
#include <iostream>
#include <sstream>

#include "opencv2/opencv.hpp"

#include "PointsDetection.hpp"
#include "EigenUtil.hpp"
#include "LLAH.hpp"

#ifdef __GNUC__
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wsign-conversion"
#endif

std::vector<std::string> LoadListFromFile(
    const std::string dirname,
    const std::string listname
)
{
    assert(IsExist(dirname+listname) &&
        "The specified file must exist"
    );
    std::ifstream ifs(dirname+listname);
    std::string line;

    std::vector<std::string> filenames;
    while(std::getline(ifs, line))
    {// for each line
        if(IsExist(dirname+line))
        {
            filenames.push_back(dirname+line);
        }
    }
    return filenames;
}

int main(int argc, char** argv)
{
    std::string dirname("../data/sample/");
    std::string filename_ref(dirname + "points_ref.png");
    std::vector<std::string> filenames_query = LoadListFromFile(dirname, "points_query_img.list");
    if(argc > 1){   dirname = argv[1];}
    else if(argc > 2){  filename_ref= dirname + argv[2];}
    else if(argc > 3){  filenames_query = LoadListFromFile(dirname, argv[3]);}
    std::string filename;

    // setup a LLAH matcher
    LLAH::Database<float> db;

    cv::Mat img, img_match;

    // Register points
    img = cv::imread(filename_ref);
    Eigen::MatrixXf points = DetectPoints<float>(img);
    std::cout << "detect " << points.cols() << " points " << std::endl;
    if(db.RegisterPoints(points))
    {
        std::cout << "Registered " << points.cols() << " points" << std::endl;

        // save the registered points
        filename = ChangeFileExtension(filename_ref, ".points");
        SavePoints<float>(points, filename);
    }
    else
    {
        std::cout << "The reference image must have at least " << db.params.n << " points" << std::endl;
    }

    // Match points
    for(std::string filename_query: filenames_query)
    {
        img = cv::imread(filename_query);

        // Detect points
        points = DetectPoints<float>(img);
        filename = ChangeFileExtension(filename_query, ".points");
        SavePoints<float>(points, filename);

        // Match the detected points
        std::cout << "Match " << points.cols() << " points..." << std::endl;
        std::vector<LLAH::Match> matches = db.MatchPoints(points);
        filename = ChangeFileExtension(filename_query, ".matches");
        LLAH::Save(matches, filename);

        // Draw the matching result
        img_match = DrawMatchedPoints(img, points,
                                      LLAH::GetIds(matches, "ref"),
                                      LLAH::GetIds(matches, "query"));
        filename = ChangeFileExtension(filename_query, "_match.png");
        cv::imwrite(filename, img_match);
    }

    return 0;
}

#ifdef __GNUC__
    #pragma GCC diagnostic pop
#endif
