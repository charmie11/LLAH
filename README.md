# LLAH

This is a simplified C++ implementation of Locally Likely Arrangement Hashing (LLAH) that is used for image retrieval and object tracking.  
Contrast to typical image features such as SIFT and ORB, LLAH computes feature vectors from keypoints distribution not from image texture although keypoints are detected from texture.  
So, LLAH should technically work with arbitrary appropriate keypoints detector.

The original LLAH was proposed by [Nakai et al.](http://imlab.jp/LLAH/)  
My implementation is based on Uchiyama's [code](http://limu.ait.kyushu-u.ac.jp/~uchiyama/me/code/UCHIYAMARKERS/index.html).  
In the code, LLAH uses an affine invariant, the ratio of two triangles' area, as its base geometric feature.  
This means that
- LLAH (the code) cannot work with severe perspective transformation.
- LLAH solves points' permutation problem in the algorithm.

## Dependency

### External dependency
This implementation heavily depends on [CMake](https://cmake.org/) and [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) library.
- Required: CMake, Eigen
- Optional: OpenCV

### Tested environment
| OS | CMake | Eigen | OpenCV |
|:--:|:--:|:--:|:--:|
| Ubuntu 18.04 | 3.10.2 | 3.3.4 | 3.2.0 |

- Eigen (3.2.92), OpenCV (3.4.2) on Ubuntu 16.04 64bit

## How to use

### How to compile
```bash
git clone https://gitlab.com/charmie11/LLAH.git
cd LLAH/build
cmake ..
make
./Test_LLAH
```

### How to in C++ code
_LLAH::Database_ class offers LLAH functionalities that register reference points and match query points to the registered reference.  
The following code shows how to register reference and match query.
The matching result is returned as a _std::vector&lt;LLAH::Match&gt;_ object.  
```
    LLAH::Database<float> db;

    // register reference points
    Eigen::MatrixXf points_ref = LoadPoints<float>(filename_ref);
    db.RegisterPoints(points_ref);

    // match query points to the reference
    Eigen::MatrixXf points_query = LoadPoints<float>(filename_query);
    std::vector<LLAH::Match> matches = db.MatchPoints(points);

    // save and show the results
    LLAH::Save(matches, filename_matches);
    for(auto match: matches)
    {
        std::cout << "reference[" << match.id_ref << "] <-> query[" << match.id_query << "]" << std::endl;
    }
```

Executing the above code, you will see the following message on the terminal.  
As you may notice, some reference points are assigned to several query points.  
```
  reference[7] <-> query[0]
  reference[3] <-> query[1]
  reference[0] <-> query[2]
  reference[10] <-> query[3]
  reference[7] <-> query[4]
  reference[8] <-> query[5]
  reference[1] <-> query[6]
  reference[8] <-> query[7]
  reference[1] <-> query[8]
  reference[12] <-> query[9]
  reference[15] <-> query[10]
```

### Offline tracking with text formated points
Run main_offline_tracking_from_files.cpp.
```
./build/LLAH_Offline_from_file DIRNAME REFNAME LISTNAME
```
_DIRNAME_ is a directory where all files are stored.  
_DIRNAME_+_REFNAME_ contains a pointset such that each line contains a 2D point information with a delimiter (default is ',').  
_DIRNAME_+_LISTNAME_ contains a set of filenames, each of which contains a query pointset.  

### Offline tracking with image formated points (Requires OpenCV)
Run main_offline_tracking_from_images.cpp.
```
./build/LLAH_Offline_from_image DIRNAME REFNAME LISTNAME
```
_DIRNAME_ is a directory where all files are stored.  
_DIRNAME_+_REFNAME_ contains an image of reference pointset.  
_DIRNAME_+_LISTNAME_ contains a set of filenames, each of which is an image of query pointset.

### Online tracking (Requires OpenCV)
Connect a camera to your PC and then run main_online_tracking.cpp.
```
./build/LLAH_Online DIRNAME
```
_DIRNAME_ is a directory where all files are stored.

## How to improve the tracking performance
LLAH uses the distribution of neighboring points as mentioned above.  
Most important parameters of LLAH is _n_ and _m_ (see Sec. 2.4 of Nakai2006 for detail).  
Simply speaking, larger _n(>m)_ and smaller _m(>4)_ improves the tracking performance against perspective transformation.  
Note that larger _n_ increases computation time exponentially.  
Empirically, their default values are set as _n = 8, m = 5_.  
You can set these parameters via constructor as
```
    int n = 9;
    int m = 5;
    LLAH::Database<float> db(n, m);
```

## References
If you use the code or part of the code in your work, please cite the following papers.
```
@inproceedings{Nakai2006,
  title={Use of affine invariants in locally likely arrangement hashing for camera-based document image retrieval},
  author={T. Nakai and K. Kise and M. Iwamura},
  booktitle={International Workshop DAS}
  pages={541--552},
  year={2006}
}
```

```
@inproceedings{Uchiyama2011,
  title={Random dot markers},
  author={Hideaki Uchiyama and Hideo Saito},
  booktitle={IEEE Virtual Reality (VR)},
  pages={35--38},
  year={2011}
}
```
